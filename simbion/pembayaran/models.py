from django.db import models
from daftar_beasiswa.models import Mahasiswa, SkemaBeasiswaAktif


class Pembayaran(models.Model):
    urutan = models.IntegerField()
    kode_skema_beasiswa = models.ForeignKey(SkemaBeasiswaAktif, models.CASCADE, db_column='kode_skema_beasiswa')
    no_urut_skema_beasiswa_aktif = models.IntegerField()
    npm = models.ForeignKey(Mahasiswa, models.CASCADE, db_column='npm')
    keterangan = models.CharField(max_length=50)
    tgl_bayar = models.DateField()
    nominal = models.IntegerField()

    class Meta:
        db_table = 'pembayaran'
        unique_together = (('urutan', 'kode_skema_beasiswa', 'no_urut_skema_beasiswa_aktif', 'npm'),)
