from django.contrib import messages
from django.db import connection, IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import PembayaranForm

response = {}
cursor = connection.cursor()


def index(request):
    response['author'] = 'Benny William Pardede'
    response['role'] = 'donatur'
    response['form_wawancara'] = PembayaranForm
    html = 'pembayaran/pembayaran.html'
    return render(request, html, response)


def tambah_pembayaran(request):
    form = PembayaranForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        urutan = request.POST['urutan']
        kode_skema = request.POST['kode_skema']
        urutan_skema = request.POST['urutan_skema']

        cursor.execute("SELECT no_urut FROM skema_beasiswa_aktif WHERE kode_skema_beasiswa = %s", [kode_skema])
        if int(urutan_skema) not in [i[0] for i in cursor.fetchall()]:
            messages.add_message(request, 40, 'Kode skema ' + kode_skema
                                 + ' tidak memiliki Urutan skema ' + urutan_skema)
            return HttpResponseRedirect('/pembayaran/')

        npm = request.POST['npm']
        keterangan = request.POST['keterangan']
        tanggal_bayar = form.cleaned_data["tanggal_bayar"]
        nominal = request.POST['nominal']

        try:
            cursor.execute("INSERT INTO pembayaran(urutan," +
                           " kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm, keterangan," +
                           " tgl_bayar, nominal) VALUES(%s,%s,%s,%s,%s,%s,%s)",
                           [urutan, kode_skema, urutan_skema, npm, keterangan, tanggal_bayar, nominal])
            messages.add_message(request, 25, 'Laporan Pembayaran berhasil ditambahkan!')
            return HttpResponseRedirect('/pembayaran/')
        except IntegrityError:
            messages.add_message(request, 40, 'Insert failed: NPM tidak terdaftar atau terdapat duplikasi')
            return HttpResponseRedirect('/pembayaran/')
    else:
        return HttpResponseRedirect('/pembayaran/')
