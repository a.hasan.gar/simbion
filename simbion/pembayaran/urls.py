from django.urls import path

from .views import *

app_name = 'pembayaran'

urlpatterns = [
    path('', index, name='index'),
    path('tambah_pembayaran', tambah_pembayaran, name='tambah_pembayaran'),
]
