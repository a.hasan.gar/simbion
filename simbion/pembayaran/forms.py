from django import forms
from django.db import connection

text_input_attrs = {
    'type': 'text',
    'class': 'form-textinput',
}

number_input_attrs = {
    'type': 'number',
    'class': 'form-numberinput',
}

select_attrs = {'class': 'form-select'}

select_date_attrs = {'class': 'form-select-date'}


def get_kode_choices():
    cursor = connection.cursor()
    cursor.execute("SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif")
    iterator = set(cursor.fetchall())
    iterator = sorted(iterator)
    res = []
    for p in iterator:
        res.append((str(p[0]), p[0]))
    return tuple(res)


def get_urutan_choices():
    cursor = connection.cursor()
    cursor.execute("SELECT no_urut FROM skema_beasiswa_aktif")
    iterator = set(cursor.fetchall())
    iterator = sorted(iterator)
    res = []
    for p in iterator:
        res.append((str(p[0]), p[0]))
    return tuple(res)


class PembayaranForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(PembayaranForm, self).__init__(*args, **kwargs)
        self.fields['kode_skema'] = forms.ChoiceField(
            label='Kode Skema Beasiswa', required=True, widget=forms.Select(attrs=select_attrs), choices=get_kode_choices())
        self.fields['urutan_skema'] = forms.ChoiceField(
            label='Nomor Urut Skema Beasiswa', required=True, widget=forms.Select(attrs=select_attrs), choices=get_urutan_choices())
        self.order_fields(["urutan", "kode_skema", "urutan_skema", "npm", "keterangan", "tanggal_bayar", "nominal"])
    urutan = forms.IntegerField(label='Urutan', required=True, widget=forms.NumberInput(attrs=number_input_attrs))
    npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs=number_input_attrs))
    keterangan = forms.CharField(label='Keterangan', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    tanggal_bayar = forms.DateField(label='Tanggal Bayar', required=True, widget=forms.SelectDateWidget(attrs=select_date_attrs))
    nominal = forms.IntegerField(label='Nominal', required=True, widget=forms.NumberInput(attrs=number_input_attrs))
