from django.db import connection
from django.shortcuts import render


def dictfetchone(cursor):
    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, cursor.fetchone()))


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


cursor = connection.cursor()
response = {}


def index(request):
    response['author'] = 'Benny William Pardede'
    response['role'] = 'pengunjung'
    cursor.execute("SELECT 1 id, * FROM pengumuman")
    pengumuman = dictfetchall(cursor)
    response['pengumuman'] = pengumuman
    html = 'pengumuman/pengumuman.html'
    return render(request, html, response)


def content(request, index):
    kode = index.split("_")[0]
    urutan = index.split("_")[1]
    response['author'] = 'Benny William Pardede'
    response['role'] = 'pengunjung'
    cursor.execute("SELECT 3 id, * FROM pengumuman " +
                   "WHERE kode_skema_beasiswa = %s AND no_urut_skema_beasiswa_aktif = %s", [kode, urutan])
    content = dictfetchone(cursor)
    response['content'] = content
    html = 'pengumuman/content.html'
    return render(request, html, response)
