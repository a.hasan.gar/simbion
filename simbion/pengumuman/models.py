from django.db import models
from daftar_beasiswa.models import Pengguna, SkemaBeasiswaAktif


class Pengumuman(models.Model):
    tanggal = models.DateField()
    no_urut_skema_beasiswa_aktif = models.ForeignKey(SkemaBeasiswaAktif, models.CASCADE, db_column='no_urut_skema_beasiswa_aktif')
    kode_skema_beasiswa = models.IntegerField()
    username = models.ForeignKey(Pengguna, models.CASCADE, db_column='username')
    judul = models.CharField(max_length=20)
    isi = models.CharField(max_length=255)

    class Meta:
        db_table = 'pengumuman'
        unique_together = (('tanggal', 'no_urut_skema_beasiswa_aktif', 'kode_skema_beasiswa', 'username'),)
