from django import forms

text_input_attrs = {
    'type': 'text',
    'class': 'form-textinput',
}


class PembayaranForm(forms.Form):
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))