from django.urls import path

from .views import index

app_name = 'login'

urlpatterns = [
    path('', index, name='index'),
]
