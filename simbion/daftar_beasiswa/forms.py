from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from .models import SkemaBeasiswa, SkemaBeasiswaAktif

JENIS_PAKET_BEASISWA = [("Prestasi Akademik", "Beasiswa Prestasi Akademik"),
						("Bantuan Belajar", "Beasiswa Bantuan Belajar"),
						("Tugas Akhir", "Beasiswa Tugas Akhir")]

error_messages = {'required': 'Tolong isi input ini.'}

text_input_attrs = {
	'type': 'text',
	'class': 'form-textinput',
}

text_area_attrs = {
	'type': 'text',
	'cols': 50,
	'rows': 4,
	'class': 'form-textarea',
	'placeholder':'Masukan deskripsi...'
}

select_attrs = {'class' : 'form-select'}

select_date_attrs = {'class' : 'form-select-date'}

class Daftar_Beasiswa_Form(forms.Form):
	kode = forms.IntegerField(label='Kode', label_suffix=' : ', required=True,
		widget=forms.TextInput(attrs=text_input_attrs))

	nama = forms.CharField(label='Nama Paket Beasiswa', label_suffix=' : ',
		required=True, widget=forms.TextInput(attrs=text_input_attrs), max_length=50)

	jenis = forms.ChoiceField(label='Jenis Paket Beasiswa', label_suffix=' : ',
		required=True, widget=forms.Select(attrs=select_attrs), choices=JENIS_PAKET_BEASISWA)

	deskripsi = forms.CharField(label='Deskripsi', label_suffix=' : ', required=True,
		widget=forms.Textarea(attrs=text_area_attrs), max_length=50)

	syarat_beasiswa = forms.CharField(label='Syarat Beasiswa', label_suffix=' : ', required=True,
		widget=forms.Textarea(attrs=text_area_attrs), max_length=50)

	def clean(self):
		super().clean()

		if 'kode' not in self.cleaned_data:
			return self.cleaned_data

		cleaned_data = self.cleaned_data
		kode_beasiswa = cleaned_data['kode']

		if not (0 <= kode_beasiswa and kode_beasiswa < (1 << 31)):
			kode_beasiswa_out_of_range_error = ValidationError(_("Kode beasiswa di luar jangkauan."))
			raise kode_beasiswa_out_of_range_error

		result = SkemaBeasiswa.objects.raw("SELECT kode FROM skema_beasiswa WHERE kode = %s LIMIT 1;", [kode_beasiswa])

		if len(list(result)) > 0:
			duplicate_kode_beasiswa_error = ValidationError(_("Duplicate kode beasiswa."))
			raise duplicate_kode_beasiswa_error

		return cleaned_data

class Tambah_Beasiswa_Aktif_Form(forms.Form):
	no_urut = forms.IntegerField(label='Nomor Urut', label_suffix=' : ', required=True,
		widget=forms.TextInput(attrs=text_input_attrs))

	tanggal_mulai_pendaftaran = forms.DateField(label='Tanggal Mulai Pendaftaran', label_suffix=' : ', required=True,
		widget=forms.SelectDateWidget(attrs=select_date_attrs))

	tanggal_tutup_pendaftaran = forms.DateField(label='Tanggal Tutup Pendaftaran', label_suffix=' : ', required=True,
		widget=forms.SelectDateWidget(attrs=select_date_attrs))

	def populate_kode_beasiswa_choices(self, nomor_identitas_donatur):
		results = SkemaBeasiswa.objects.raw("SELECT kode FROM skema_beasiswa WHERE nomor_identitas_donatur = %s", \
			[nomor_identitas_donatur])

		choices = []
		for row in results:
			choices.append((row.kode, row.kode))

		self.fields["kode_beasiswa"] = forms.ChoiceField(label='Kode Beasiswa', label_suffix=' : ',
		required=True, widget=forms.Select(attrs=select_attrs), choices=choices)

		self.order_fields(["kode_beasiswa", "no_urut", "tanggal_mulai_pendaftaran", "tanggal_tutup_pendaftaran"])

	def clean(self):
		super().clean()

		# no_urut is not valid, skip cleaning
		if 'no_urut' not in self.cleaned_data:
			return self.cleaned_data

		cleaned_data = self.cleaned_data
		no_urut = cleaned_data['no_urut']
		kode_beasiswa = self.data['kode_beasiswa']

		try:
			kode_beasiswa = int(kode_beasiswa)
		except:
			raise ValidationError(_("Kode beasiswa bukan angka"))

		if not (0 <= no_urut and no_urut < (1 << 31)):
			raise ValidationError(_("Nomor urut di luar jangkauan."))

		if not (0 <= kode_beasiswa and kode_beasiswa < (1 << 31)):
			raise ValidationError(_("Kode beasiswa di luar jangkauan."))

		# assert exist skema_beasiswa
		results = SkemaBeasiswa.objects.raw("SELECT kode FROM skema_beasiswa WHERE kode = %s LIMIT 1;", [kode_beasiswa])
		if len(list(results)) == 0:
			raise ValidationError(_("Tidak ada Skema Beasiswa terkait."))

		# assert not duplicate skema_beasiswa_aktif
		results = SkemaBeasiswaAktif.objects.raw("SELECT 1 id FROM skema_beasiswa_aktif WHERE \
			kode_skema_beasiswa = %s AND no_urut = %s LIMIT 1;", [kode_beasiswa, no_urut])

		if len(list(results)) > 0:
			raise ValidationError(_("Duplicate skema beasiswa aktif."))

		if cleaned_data["tanggal_mulai_pendaftaran"] > cleaned_data["tanggal_tutup_pendaftaran"]:
			raise ValidationError(_("Tanggal mulai pendaftaran harus sebelum tanggal tutup pendaftaran."))

		return cleaned_data
