from django.contrib import messages
from django.db import connection
from django.shortcuts import render, redirect
import datetime

from .forms import Daftar_Beasiswa_Form, Tambah_Beasiswa_Aktif_Form
from .models import SkemaBeasiswa

DEFAULT_NOMOR_DONATUR = "1"
response = {"author" : "Ahmad Hasan Siregar", "role" : "donatur"}

def daftar_beasiswa(request):
	if request.method == 'POST':
		form = Daftar_Beasiswa_Form(request.POST)
		if form.is_valid():
			save_beasiswa_baru(form)
			messages.success(request, "Berhasil membuat paket beasiswa baru.")
			return redirect('index')

	else:
		form = Daftar_Beasiswa_Form()

	html = "daftar_beasiswa/daftar_beasiswa.html"
	response["daftar_beasiswa_form"] = form
	return render(request, html, response)

def tambah_beasiswa_aktif(request):
	if request.method == 'POST':
		form = Tambah_Beasiswa_Aktif_Form(request.POST)
		if form.is_valid():
			save_beasiswa_aktif_baru(form)
			messages.success(request, "Berhasil menambahkan skema beasiswa aktif.")
			return redirect('index')
	else:
		form = Tambah_Beasiswa_Aktif_Form()

	form.populate_kode_beasiswa_choices(DEFAULT_NOMOR_DONATUR)
	html = 'daftar_beasiswa/tambah_beasiswa_aktif.html'
	response["tambah_beasiswa_aktif_form"] = form
	return render(request, html, response)

def save_beasiswa_baru(form):
	with connection.cursor() as cursor:
		# insert the new skema beasiswa first
		cursor.execute("INSERT INTO skema_beasiswa(kode, nama, jenis, deskripsi, nomor_identitas_donatur) \
			VALUES (%s, %s, %s, %s, %s);",
		[form.cleaned_data["kode"], form.cleaned_data["nama"], form.cleaned_data["jenis"],
		form.cleaned_data["deskripsi"], DEFAULT_NOMOR_DONATUR])

		# insert syarat beasiswa
		cursor.execute("INSERT INTO syarat_beasiswa(kode_beasiswa, syarat) VALUES(%s, %s);", \
			[form.cleaned_data["kode"], form.cleaned_data["syarat_beasiswa"]])

def save_beasiswa_aktif_baru(form):
	tgl_mulai = form.cleaned_data["tanggal_mulai_pendaftaran"]
	tgl_tutup = form.cleaned_data["tanggal_tutup_pendaftaran"]

	# ignore if today is before tgl_mulai
	status = "Dibuka" if datetime.date.today() <= tgl_tutup else "Ditutup"
	periode_penerimaan = tgl_mulai.year

	with connection.cursor() as cursor:
		cursor.execute("INSERT INTO skema_beasiswa_aktif(kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, \
			tgl_tutup_pendaftaran, periode_penerimaan, status) VALUES (%s, %s, %s, %s, %s, %s);",
			[form.data['kode_beasiswa'], form.cleaned_data['no_urut'], tgl_mulai, tgl_tutup, periode_penerimaan,
			status])