from django.shortcuts import render
from daftar_beasiswa.models import SkemaBeasiswa, SkemaBeasiswaAktif, SyaratBeasiswa

response = {"author" : "Ahmad Hasan Siregar", "role" : "mahasiswa"}

def index(request):
	response["beasiswa_list"] = get_list_of_beasiswa()
	html = "lihat_beasiswa/lihat_beasiswa.html"
	return render(request, html, response)

def detail_beasiswa(request, kode_skema):
	html = "lihat_beasiswa/detail_beasiswa.html"
	skema_beasiswa = get_skema_beasiswa(kode_skema)

	if skema_beasiswa != None:
		syarat_beasiswa = get_syarat_beasiswa(skema_beasiswa.kode)

		data = {"nama" : skema_beasiswa.nama, "kode" : skema_beasiswa.kode, "jenis" : skema_beasiswa.jenis,
				"donatur" : skema_beasiswa.nomor_identitas_donatur.nomor_identitas,
				"deskripsi" : skema_beasiswa.deskripsi, "syarat" : syarat_beasiswa}
	else:
		data = None

	response["beasiswa"] = data
	return render(request, html, response)

def get_skema_beasiswa(kode_skema):
	query = "SELECT * FROM skema_beasiswa WHERE kode = " + str(kode_skema) + ";"
	skema_beasiswa = SkemaBeasiswa.objects.raw(query)

	try:
		return skema_beasiswa[0]
	except IndexError:
		return None

def get_list_of_beasiswa():
	beasiswa_list = []
	query = "SELECT 1 id, * FROM skema_beasiswa_aktif ORDER BY tgl_tutup_pendaftaran DESC"
	no = 1

	for beasiswa in SkemaBeasiswaAktif.objects.raw(query):
		data = {"no" : no, "kode_skema" : beasiswa.kode_skema_beasiswa.kode,
				"nama" : beasiswa.kode_skema_beasiswa.nama,
				"tanggal_tutup" : beasiswa.tgl_tutup_pendaftaran.strftime("%d/%m/%Y"),
				"status" : beasiswa.status, "jumlah_pendaftar" : beasiswa.jumlah_pendaftar}

		beasiswa_list.append(data)
		no += 1

	return beasiswa_list

def get_syarat_beasiswa(kode_beasiswa):
	query = "SELECT 1 id, syarat FROM syarat_beasiswa WHERE kode_beasiswa = " + str(kode_beasiswa) + ";"
	syarat_beasiswa = []
	for syarat in SyaratBeasiswa.objects.raw(query):
		syarat_beasiswa.append(syarat.syarat)

	return syarat_beasiswa