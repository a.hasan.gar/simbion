from django.contrib import messages
from django.db import connection, IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import WawancaraForm

response = {}
cursor = connection.cursor()


def index(request):
    response['author'] = 'Benny William Pardede'
    response['role'] = 'Administrator'
    response['form_wawancara'] = WawancaraForm
    html = 'wawancara/wawancara.html'
    return render(request, html, response)


def tambah_tempat(request):
    form = WawancaraForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        kode = request.POST['kode']
        nama = request.POST['nama']
        lokasi = request.POST['lokasi']
        try:
            cursor.execute("INSERT INTO tempat_wawancara(kode, nama, lokasi) " +
                           "VALUES(%s,%s,%s)", [kode, nama, lokasi])
            messages.add_message(request, 25, 'Tempat wawancara berhasil ditambahkan!')
            return HttpResponseRedirect('/wawancara/')
        except IntegrityError:
            messages.add_message(request, 40, 'Kode tempat ' + kode + ' sudah digunakan!')
            return HttpResponseRedirect('/wawancara/')
    else:
        return HttpResponseRedirect('/wawancara/')
