from django.urls import path

from .views import *

app_name = 'wawancara'

urlpatterns = [
    path('', index, name='index'),
    path('tambah_tempat', tambah_tempat, name='tambah_tempat'),
]
