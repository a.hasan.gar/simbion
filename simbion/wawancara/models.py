from django.db import models


class TempatWawancara(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    lokasi = models.CharField(max_length=50)

    class Meta:
        db_table = 'tempat_wawancara'
