--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.15
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: simbion; Type: SCHEMA; Schema: -; Owner: db041
--

CREATE SCHEMA simbion;


ALTER SCHEMA simbion OWNER TO db041;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = simbion, pg_catalog;

--
-- Name: total_pembayaran_ft(); Type: FUNCTION; Schema: simbion; Owner: db041
--

CREATE FUNCTION total_pembayaran_ft() RETURNS trigger
    LANGUAGE plpgsql
    AS $$  
    DECLARE
        nomina_pembayaran INT;

    BEGIN
        IF (TG_OP = 'INSERT') THEN
            nomina_pembayaran = NEW.nominal;

            UPDATE SKEMA_BEASISWA_AKTIF AS S
            SET total_pembayaran = S.total_pembayaran + nomina_pembayaran
            WHERE S.kode_skema_beasiswa = NEW.kode_skema_beasiswa AND
                S.no_urut = NEW.no_urut_skema_beasiswa_aktif;

        END IF;

        RETURN NEW;
    END;
$$;


ALTER FUNCTION simbion.total_pembayaran_ft() OWNER TO db041;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE admin (
    username character varying(20) NOT NULL
);


ALTER TABLE admin OWNER TO db041;

--
-- Name: donatur; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE donatur (
    nomor_identitas character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    npwp character(20) NOT NULL,
    no_telp character varying(20),
    alamat character varying(50) NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE donatur OWNER TO db041;

--
-- Name: individual_donor; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE individual_donor (
    nik character(16) NOT NULL,
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE individual_donor OWNER TO db041;

--
-- Name: mahasiswa; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE mahasiswa (
    npm character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    no_telp character varying(20),
    alamat_tinggal character varying(50) NOT NULL,
    alamat_domisili character varying(50) NOT NULL,
    nama_bank character varying(50) NOT NULL,
    no_rekening character varying(20) NOT NULL,
    nama_pemilik character varying(20) NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE mahasiswa OWNER TO db041;

--
-- Name: pembayaran; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE pembayaran (
    urutan integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    npm character varying(20) NOT NULL,
    keterangan character varying(50) NOT NULL,
    tgl_bayar date NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE pembayaran OWNER TO db041;

--
-- Name: pendaftaran; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE pendaftaran (
    no_urut integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    npm character varying(20) NOT NULL,
    waktu_daftar timestamp without time zone NOT NULL,
    status_daftar character varying(20) NOT NULL,
    status_terima character varying(20) NOT NULL
);


ALTER TABLE pendaftaran OWNER TO db041;

--
-- Name: pengguna; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE pengguna (
    username character varying(20) NOT NULL,
    password character varying(20) NOT NULL,
    role character varying(20) NOT NULL
);


ALTER TABLE pengguna OWNER TO db041;

--
-- Name: pengumuman; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE pengumuman (
    tanggal date NOT NULL,
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    username character varying(20) NOT NULL,
    judul character varying(20) NOT NULL,
    isi character varying(255) NOT NULL
);


ALTER TABLE pengumuman OWNER TO db041;

--
-- Name: riwayat_akademik; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE riwayat_akademik (
    no_urut integer NOT NULL,
    npm character varying(20) NOT NULL,
    semester character(1) NOT NULL,
    tahun_ajaran character(9) NOT NULL,
    jumlah_sks integer NOT NULL,
    ips double precision NOT NULL,
    lampiran character varying(50) NOT NULL
);


ALTER TABLE riwayat_akademik OWNER TO db041;

--
-- Name: skema_beasiswa; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE skema_beasiswa (
    kode integer NOT NULL,
    nama character varying(50) NOT NULL,
    jenis character varying(20) NOT NULL,
    deskripsi character varying(50) NOT NULL,
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE skema_beasiswa OWNER TO db041;

--
-- Name: skema_beasiswa_aktif; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE skema_beasiswa_aktif (
    kode_skema_beasiswa integer NOT NULL,
    no_urut integer NOT NULL,
    tgl_mulai_pendaftaran date NOT NULL,
    tgl_tutup_pendaftaran date NOT NULL,
    periode_penerimaan character varying(50) NOT NULL,
    status character varying(20) NOT NULL,
    total_pembayaran integer DEFAULT 0 NOT NULL,
    jumlah_pendaftar integer DEFAULT 0
);


ALTER TABLE skema_beasiswa_aktif OWNER TO db041;

--
-- Name: syarat_beasiswa; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE syarat_beasiswa (
    kode_beasiswa integer NOT NULL,
    syarat character varying(50) NOT NULL
);


ALTER TABLE syarat_beasiswa OWNER TO db041;

--
-- Name: tempat_wawancara; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE tempat_wawancara (
    kode integer NOT NULL,
    nama character varying(50) NOT NULL,
    lokasi character varying(50) NOT NULL
);


ALTER TABLE tempat_wawancara OWNER TO db041;

--
-- Name: wawancara; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE wawancara (
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    jadwal timestamp without time zone NOT NULL,
    kode_tempat_wawancara integer NOT NULL
);


ALTER TABLE wawancara OWNER TO db041;

--
-- Name: yayasan; Type: TABLE; Schema: simbion; Owner: db041
--

CREATE TABLE yayasan (
    no_sk_yayasan character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    no_telp_cp character varying(20),
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE yayasan OWNER TO db041;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY admin (username) FROM stdin;
tbenettolo0
dnowlan1
kgirdwood2
mmcinnerny3
mjanoch4
\.


--
-- Data for Name: donatur; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY donatur (nomor_identitas, email, nama, npwp, no_telp, alamat, username) FROM stdin;
1	emccarty0@lulu.com	Elspeth McCarty	28-420-531-2-092-651	+7985244741448	1 Di Loreto Alley	alambrookn
2	lshapland1@naver.com	Livia Shapland	65-141-933-0-899-578	+9685891360370	473 Spaight Pass	azambont
3	gosgardby2@51.la	Grete Osgardby	61-369-118-3-043-623	+2384661204959	50 Quincy Road	blindbergv
4	cbraemer3@apache.org	Clevie Braemer	98-193-506-6-099-776	+5386728520178	8 Mendota Pass	akohrty
5	lmartinovic4@tripadvisor.com	Leisha Martinovic	81-240-721-4-971-878	+3682342878852	15840 Blackbird Crossing	bcooley16
6	ebate5@theglobeandmail.com	Earl Bate	73-979-003-7-953-738	+6086821189367	1 Tony Point	bburkett1e
7	mperazzo6@webeden.co.uk	Matteo Perazzo	60-336-638-5-150-465	+2288462947044	3638 Express Point	cgarwood1q
8	dskeleton7@techcrunch.com	Donny Skeleton	99-127-210-4-300-758	+0489647304003	14 Myrtle Junction	cbilton1s
9	nseally8@goodreads.com	Norina Seally	86-249-510-8-905-266	+5387866734942	6 Union Alley	bholmyard26
10	fbrouard9@google.com	Friederike Brouard	61-023-321-2-238-361	+4780066078262	161 Maywood Junction	araoult28
11	vkrafta@ucoz.ru	Vilhelmina Kraft	41-896-448-0-121-310	+6880999676745	2288 Main Pass	bfransinelli2n
12	aroubeixb@ustream.tv	Ashlen Roubeix	37-352-934-9-899-197	+0185625102333	9010 Clyde Gallagher Lane	blavers2v
13	pbarnbyc@livejournal.com	Parke Barnby	87-132-995-2-065-295	+8782197967692	15598 Briar Crest Terrace	cgilleon38
14	bedmonstoned@storify.com	Barnaby Edmonstone	00-993-016-5-022-835	+4984035088805	860 Melby Crossing	bhallward3i
15	umatchese@nba.com	Ursola Matches	85-763-917-7-349-782	+8583508211858	4 Barby Trail	cbromet3t
16	glugsdinf@360.cn	Gretna Lugsdin	45-656-092-4-019-881	+7880469063312	5 Homewood Crossing	bberrick3u
17	kmiddlebrookg@wunderground.com	Keelia Middlebrook	49-888-009-2-662-778	+4488823714986	903 La Follette Point	cmatyashev3y
18	bdeh@dyndns.org	Budd De Mitri	75-257-668-8-873-681	+3289802052193	0 Garrison Center	abanbrigge3z
19	rmorysoni@addthis.com	Rab Moryson	41-968-033-6-436-769	+7688420895666	0 Blackbird Trail	chenker43
20	mpudgej@geocities.com	Meg Pudge	50-990-698-9-424-498	+2181125146531	734 Kinsman Terrace	bcardiff45
21	mgearek@rakuten.co.jp	Mateo Geare	10-852-836-5-582-322	+8883555073394	30628 Milwaukee Alley	cjepson49
22	bpotburyl@drupal.org	Bale Potbury	98-682-057-4-777-583	+9885528752054	3 Columbus Terrace	avacher4d
23	clem@linkedin.com	Crin Le Noir	88-624-920-7-304-840	+3383895267651	6288 Kenwood Alley	cflaubert4m
24	jstihln@stanford.edu	Jorry Stihl	55-480-390-7-387-645	+0680464707381	5 Warrior Avenue	arizzi50
25	efrancecioneo@sohu.com	Edin Francecione	41-727-185-2-841-747	+7782096633983	7 American Pass	cchisholm52
26	lvipanp@joomla.org	Lenee Vipan	28-164-734-7-250-753	+0882466946685	683 Nobel Plaza	aomrod53
27	felamq@uiuc.edu	Federico Elam	47-229-356-3-147-793	+0982649251134	0004 Homewood Plaza	bbracknell5d
28	wconninghamr@google.com.br	Ware Conningham	37-365-521-5-998-650	+7082367524139	911 Muir Point	ajoanic5e
29	ggerrelts@psu.edu	Georgianne Gerrelt	74-999-460-6-532-834	+7489436758043	016 Arapahoe Circle	amaydway5f
30	mdabrowskit@forbes.com	Mariellen Dabrowski	41-223-710-8-374-119	+2984250749432	474 Cordelia Point	bmcgeever5o
31	mtuohyu@icq.com	Marlo Tuohy	42-295-387-9-480-190	+6982252960721	73856 Lakewood Gardens Court	agallie5r
32	mbleierv@sciencedirect.com	Mersey Bleier	07-741-715-7-932-261	+1785263874041	0 Park Meadow Terrace	atocher5w
33	vbranneyw@amazonaws.com	Valli Branney	72-183-677-1-141-892	+5685387848016	10 Blackbird Drive	aratray5x
34	edetheridgex@csmonitor.com	Emmey Detheridge	10-253-573-7-234-317	+7987565636713	37 Jenna Hill	cboden60
35	scrockeny@nyu.edu	Scarface Crocken	12-406-691-9-021-495	+9486372805200	0379 Orin Road	cdurrad61
36	grogersonz@ox.ac.uk	Gussi Rogerson	07-429-473-8-136-787	+8883570260285	2382 Magdeline Crossing	adurrand66
37	lstellino10@sfgate.com	Lesley Stellino	89-114-250-0-910-961	+4589845803426	87 Canary Trail	athompson6g
38	kfulstow11@t.co	Kaine Fulstow	74-485-952-2-813-701	+7180052683702	10 Monterey Circle	aheller6s
39	apriestner12@yale.edu	Austina Priestner	67-812-942-7-576-067	+1084578695113	21000 Schiller Plaza	awalcar6v
40	ddagworthy13@t.co	Dianne Dagworthy	55-425-081-4-622-916	+5182903437044	9 Rowland Court	blidgertwood6y
41	dhaquard14@istockphoto.com	Darnall Haquard	90-573-375-5-629-547	+7787987223984	949 Waywood Alley	bbarracks70
42	odu15@theguardian.com	Oralie Du Hamel	62-151-330-7-407-155	+8483367893093	9 Manley Circle	afrift77
43	ssimmon16@google.com	Salomone Simmon	75-482-036-8-867-100	+9182693069752	622 Village Junction	cmathevet7d
44	hnewbury17@twitpic.com	Hillary Newbury	63-734-872-3-821-607	+0586088467786	978 Barnett Junction	cludovici7p
45	edover18@scribd.com	Efrem Dover	38-586-632-4-354-875	+0584184606595	647 Elgar Way	cgirauld7r
46	abulstrode19@businessinsider.com	Aube Bulstrode	96-623-756-7-383-457	+9384845851435	7411 South Road	bcakes7s
47	whamblin1a@pen.io	Whitby Hamblin	75-948-846-7-406-702	+8688919414963	6786 Clarendon Crossing	aferrao7t
48	mflew1b@chron.com	Marven Flew	59-354-384-2-314-382	+5885007038604	1 Garrison Place	asodeau7w
49	egreatrakes1c@drupal.org	Evelin Greatrakes	68-946-935-8-707-535	+6284786293735	28053 Kim Terrace	cmccollum7z
50	espringate1d@g.co	Erich Springate	64-271-573-5-206-329	+5189349869157	48735 Scoville Terrace	bfergusson89
\.


--
-- Data for Name: individual_donor; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY individual_donor (nik, nomor_identitas_donatur) FROM stdin;
1526026997926560	30
8282471361255471	12
5939032707284800	31
5659522502073897	27
3172787577077830	44
7001878408396770	21
2256985485205814	34
1547056290604016	16
3780452153131067	11
3043941275414827	13
4155101013061652	33
7243151281558224	47
8390561462103221	48
7489772764338043	1
2393664086158990	24
5019532817348266	19
1118974795457148	14
1476791659620588	43
1924737481642151	9
7191037692361513	39
\.


--
-- Data for Name: mahasiswa; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY mahasiswa (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) FROM stdin;
1306133042	mfarrants0@gizmodo.com	Layne Woodcroft	+5300268934486	6776 Bayside Court	73046 Summerview Court	Kesawan	2408 26560	Melody Farrants	dmilkin5
1806827299	esimmank1@trellian.com	Patty Duberry	+2357763359084	0 Muir Trail	4 Ridge Oak Way	DBS Indonesia	4656 17487	Eugene Simmank	sroskeilly6
1706374528	hthroughton2@deliciousdays.com	Nealon Scutts	+5970189389324	52 Leroy Park	39785 Lakeland Court	DBS Indonesia	9276 38063	Harrie Throughton	echantrell7
1506994587	paphale3@woothemes.com	Curr Croser	+8542493175725	563 Eastlawn Street	5656 Waywood Place	Artha Graha Internasional	8124 95673	Paola Aphale	mlorkins8
1306413101	dleates4@blog.com	Cathryn Taree	+8865743969720	17474 Hanover Plaza	909 Clemons Drive	Mayapada	4522 19843	Dilan Leates	ypilley9
1406425546	bbiasi5@wp.com	Ryun Impleton	+8373669849598	45743 Rigney Park	9099 Talisman Plaza	Bank Tabungan Negara	4803 55980	Bronny Biasi	gslingera
1306254708	aspratt6@wordpress.org	Barry Hischke	+4074767481258	5606 Texas Avenue	953 Meadow Vale Parkway	Artha Graha Internasional	3198 66469	Arlana Spratt	ghubaneb
1706443462	iwebborn7@bravesites.com	Lance Silveston	+5900509441283	39 Bunker Hill Center	8 Sommers Hill	Central Asia	4695 25016	Ignacius Webborn	nrotherac
1306151865	nsenner8@census.gov	Billie Humes	+9144166789094	4561 1st Parkway	8 Waywood Terrace	Central Asia	2629 17976	Nicholas Senner	wonionsd
1206420944	wgendrich9@nba.com	Giustina Raspison	+7592839451208	52039 Marcy Avenue	102 Little Fleur Avenue	Bank Negara Indonesia	4226 31791	Wadsworth Gendrich	sbraistede
1806318404	lvittlea@blogger.com	Susanne Ransome	+0621260901863	77570 Prairieview Court	7760 Mandrake Avenue	Bank Tabungan Negara	1278 81110	Lizzie Vittle	mmaffyf
1706358415	tgoveyb@hud.gov	Sal de Juares	+4687919670691	4194 Loomis Point	9295 Sunbrook Hill	OCBC NISP	0563 27350	Tobie Govey	cweighg
1206240645	bdic@cyberchimps.com	Camellia MacCaffrey	+3021865767607	09476 Bluestem Lane	2994 Dapin Park	Muamalat Indonesia	0456 41028	Bendick Di Iorio	dwindrassh
1206526391	mtordiffed@nature.com	Gus Frangello	+8576319988592	961 Eastlawn Junction	740 Oriole Trail	Artha Graha Internasional	1245 16052	Matthiew Tordiffe	mpolleyi
1206301664	bkiddiee@spiegel.de	Kevina Mossop	+1331757851422	33 Schmedeman Avenue	3629 Hanover Place	Index Selindo	7959 99691	Benjy Kiddie	pmeadwayj
1606362154	kethertonf@usatoday.com	Ketty Becket	+1350870278316	54623 Harper Pass	69 Montana Pass	Sinarmas	8461 20802	Kristine Etherton	mfarendenk
1106280943	rgoodnowg@sohu.com	Kimberlyn Lockney	+1109890547629	08877 Luster Place	037 Florence Park	Kesawan	0350 39338	Rosabel Goodnow	jjohananoffl
1106115142	egouldstoneh@stumbleupon.com	Darby Wedon	+3887511072890	13 Jackson Place	964 Shoshone Trail	ICB Bumiputera	7227 07074	Elliot Gouldstone	ddybald4j
1506452868	lgainsfordi@sphinn.com	Bendite Trevorrow	+4308553940524	56 Meadow Valley Way	465 Oakridge Crossing	Maspion	1291 91160	Lilyan Gainsford	ttschersich4k
1806624411	lamdohrj@xing.com	Ewen Scarce	+9575877820434	9 Linden Parkway	0 Pond Point	Central Asia	9340 00526	Laverne Amdohr	mcreamen4l
1006871284	lfarlambek@boston.com	Katine Quantrill	+5029283090451	56735 Melby Lane	15 Gulseth Terrace	Metro Express	0541 37787	Laverne Farlambe	gbrockton4n
1306169650	jrushel@geocities.com	Randell Goadsby	+0764150500711	76102 Comanche Alley	18479 Blue Bill Park Drive	ICBC Indonesia	7551 01866	Jeannie Rushe	durch4o
1906900502	llawdenm@netlog.com	Agosto Habbon	+3547868134260	2527 Manufacturers Terrace	4 Logan Circle	Mayapada	3767 35076	Lorne Lawden	lroberts4p
1606847536	hwebben@elpais.com	Terencio Schubart	+3966039024694	6 Bluejay Road	73 Susan Crossing	SBI Indonesia	3334 02601	Hermione Webbe	pgodly4q
1006447470	alossmano@yahoo.co.jp	Prent Roches	+6151258103841	3 Pleasure Court	2633 Vidon Avenue	Kesawan	7492 21335	Allys Lossman	fcremer4r
1206296516	ylahyp@desdev.cn	Lorne Ralph	+7299203960558	8897 Arrowood Parkway	76059 American Drive	Mandiri	7875 16163	Yvor Lahy	lsteketee4s
1006340824	pmcgloughlinq@geocities.jp	Benito Gratten	+6890994661852	3 Fuller Alley	0 Iowa Court	Central Asia	9851 71493	Pieter McGloughlin	jfirmage4t
1506238587	rstollbergr@wix.com	Clem Jickles	+0412986481942	62 Pine View Center	99755 Miller Avenue	Bumi Arta	9085 36184	Ruth Stollberg	crapelli4u
1406203323	cchurchleys@businesswire.com	Eadith Huyghe	+1356048749553	9833 Service Hill	1968 Straubel Street	Bank Negara Indonesia	0661 68784	Cloris Churchley	ldeviney4v
1606998965	agartsydet@dailymotion.com	Cass Stelfax	+2630921358969	0 Marcy Trail	19961 Anthes Parkway	Index Selindo	3998 86348	Audre Gartsyde	dbordessa4w
1906245803	vpietrusiaku@ft.com	Kearney Lonsbrough	+5344737773628	715 Emmet Parkway	9 Melvin Trail	Artha Graha Internasional	2301 54483	Vere Pietrusiak	mdubery4x
1406452300	ncoyev@ft.com	Katina Pleasance	+3094292484065	92205 Crescent Oaks Parkway	8 Walton Park	Bank Negara Indonesia	5684 68352	Neysa Coye	ptills4y
1806025248	pwildorw@blogtalkradio.com	Niki Sneesby	+9646013016963	1647 Nancy Park	47 Dexter Alley	DBS Indonesia	2660 18336	Pearl Wildor	mspender4z
1406802924	fflasbyx@netvibes.com	Mady Cato	+5483598976721	0110 Tennessee Court	7556 Mosinee Parkway	OCBC NISP	8631 07035	Fran Flasby	mfranceschi51
1106941116	fbeasanty@ustream.tv	Evelina Santarelli	+9299276726756	283 Burning Wood Center	98 Anniversary Street	Bank Rakyat Indonesia	6406 10809	Forbes Beasant	tblamphin54
1806273566	ssebringz@spotify.com	Kent Roj	+8579127704215	3383 Algoma Terrace	45 Knutson Street	SBI Indonesia	6713 38794	Smitty Sebring	kcornejo55
1106566949	waymer10@bloomberg.com	Pauline Blacklidge	+4977119335718	865 Morning Alley	7 Clove Court	OCBC NISP	6572 83918	Wit Aymer	draymond56
1906883395	mlangabeer11@tumblr.com	Mel Klauber	+7759502095245	685 Killdeer Drive	2730 West Junction	Bank Tabungan Negara	3100 60243	Myron Langabeer	zglaum57
1006124179	tmctrustam12@ox.ac.uk	Torie Geelan	+1384416796250	9 Hovde Alley	19 Golf View Place	Central Asia	9335 33918	Tami McTrustam	qmeecherm
1806974954	balbertson13@opensource.org	Lilyan Prandoni	+5744853339009	9 Gateway Court	0994 Knutson Trail	Mayapada	3356 85847	Brittaney Albertson	zthickinso
1606189532	eadair14@addtoany.com	Freddy O'Cullinane	+8876254062756	71 Pankratz Parkway	508 Ridgeview Alley	OCBC NISP	8771 54246	Evelyn Adair	jmichelinp
1706002472	cluis15@engadget.com	Felice Askie	+8864269881472	4761 Goodland Center	4 Stoughton Way	Permata	1849 99207	Cicily Luis	jduerdenq
1606166321	fmilazzo16@wikia.com	Garreth Halley	+7380970615730	8158 Eliot Drive	5 Rusk Junction	Bumi Arta	2509 80922	Fiona Milazzo	jwellanr
1206914821	spellingar17@state.tx.us	Alfie Bienvenu	+0303817499040	26 Brickson Park Park	627 Vera Lane	ICBC Indonesia	9761 63854	Stesha Pellingar	msmithens
1906347959	ereape18@sohu.com	Rora Teager	+3610916899291	56910 Golden Leaf Street	493 Grim Pass	Agroniaga	3276 98295	Esra Reape	shuygheu
1806390205	hgaishson19@zimbio.com	Petronella Attree	+1919146419173	2 Sunfield Crossing	98 Sheridan Trail	Kesawan	5751 83257	Hewitt Gaishson	pbourchierw
1106725303	iwheelwright1a@jiathis.com	Ty Goodey	+0281323310351	5435 Kipling Crossing	31 Brentwood Center	Maybank	8968 66124	Ingeborg Wheelwright	pspearex
1806239573	kmalloy1b@discuz.net	Terza Hegley	+0156874699503	66824 Bluejay Lane	2 Village Green Street	BNP Paribas Indonesia	2609 73811	Kalil Malloy	maishz
1606517541	dchrispin1c@elpais.com	Walther Grimble	+9112577695790	2836 Lakeland Place	0543 Garrison Junction	Kesawan	3333 53022	Deane Chrispin	jcamilletti10
1406633378	cmacwhan1d@squidoo.com	Valene Breward	+8936450631515	238 Brown Terrace	67636 Anderson Pass	OCBC NISP	9469 74316	Clayborn MacWhan	sdesavery11
1706698635	bcamois1e@dropbox.com	Elton Pettingill	+3112662610397	3806 Tomscot Street	84649 Mcguire Alley	Mayapada	7507 49820	Bernette Camois	hmaclise12
1306238740	gdragoe1f@tinyurl.com	Patricia Tott	+9734894352883	3308 Daystar Circle	5447 Kennedy Plaza	Muamalat Indonesia	9394 92902	Gardner Dragoe	jcaine13
1106405560	lpittam1g@scribd.com	Silvia Mackrell	+8208823376382	61 Delaware Way	15 Londonderry Hill	BNP Paribas Indonesia	3852 87420	Lizbeth Pittam	mpauling14
1506041623	mcraighill1h@technorati.com	Tedie Goldsack	+2174071488644	9 Independence Trail	91696 Buena Vista Point	OCBC NISP	8607 99314	Myrvyn Craighill	wstopper15
1206467185	dlepere1i@is.gd	Tristam Sturgeon	+9815471247724	1230 Cordelia Alley	04 Waubesa Terrace	DBS Indonesia	0426 81623	Daryl Lepere	wchazelle17
1406838692	bspurdens1j@washington.edu	Booth Spratt	+3321493318571	10863 Columbus Parkway	56 Eastwood Drive	Bumi Arta	6556 06265	Belita Spurdens	tsneezum18
1906237256	ciddon1k@fotki.com	Brittan Coils	+8635133982971	84845 Forest Dale Point	46208 Sutherland Street	Bumi Arta	9402 86828	Candra Iddon	jhallmark19
1706726175	ematy1l@hc360.com	Darbee Grubb	+8876657438343	83 Everett Road	13 4th Drive	Bank Tabungan Negara	0281 60526	Elizabeth Maty	fbooton1a
1506551420	modonovan1m@histats.com	Arv McCrow	+8576012887453	379 Sutteridge Point	49568 Superior Park	Agroniaga	6247 08184	Magdalen O'Donovan	sbowie1b
1706452587	khuncoot1o@diigo.com	Michal Paolino	+2812416110730	9539 Cody Junction	9626 Gulseth Street	Barclays Indonesia	1586 95606	Kristen Huncoot	toldroyde1d
1206265578	hmac1n@weebly.com	Jordana Daverin	+6665588010338	5 Mosinee Court	4 Cardinal Terrace	DBS Indonesia	6843 45905	Halsey Pheadair	dkenwell1c
1906772612	aseagar1p@unc.edu	Gregorio Moulder	+1358710479349	59567 Petterle Lane	23764 Melody Pass	CIMB Niaga	2210 35640	Agnola Seagar	dmacshane1f
1706861852	dsproul1q@squarespace.com	Keely Truitt	+7061764962323	3 Kropf Street	3389 Continental Circle	SBI Indonesia	7964 72499	Doreen Sproul	lgawne1g
1106824845	kbrizell1r@who.int	Benny Frankham	+3745442296908	42100 Hansons Crossing	621 Schurz Road	Bank Tabungan Negara	8279 16703	Kessiah Brizell	mboddymead1h
1206762117	vnicol1s@globo.com	Alayne Strickland	+5069757562977	5 Old Shore Parkway	39 Mariners Cove Park	Sinarmas	6397 50286	Veradis Nicol	htwiddy1i
1306476813	ahoulson1t@mapquest.com	Tirrell Childers	+5569588744658	89 Clemons Drive	729 Jay Crossing	Mandiri	6731 90746	Anne Houlson	wteek1j
1706603949	cpenk1u@mapy.cz	Evelyn Maken	+1919283944917	4842 Division Plaza	7767 Clarendon Parkway	Central Asia	2018 67649	Candace Penk	mwarboy1k
1406227772	cscottrell1v@un.org	Giustina Colum	+7942349595067	610 Russell Plaza	6 La Follette Hill	Index Selindo	0117 03005	Cosimo Scottrell	wmcgifford1l
1506479098	jowtram1w@google.ca	Donetta Bradnam	+8360227451857	627 Arrowood Center	16097 Anzinger Point	Commonwealth	0643 56922	Jonathon Owtram	gsyphas1m
1206781746	sbowdon1x@shinystat.com	Andreas Blackborn	+6267147921884	879 Forest Run Avenue	0 Old Shore Road	Danamon	9223 37923	Sheffie Bowdon	jcolebourne1n
1006671469	thinrich1y@washingtonpost.com	Solly Risen	+1962541244906	62 Upham Parkway	98109 Welch Court	Bank Negara Indonesia	2979 68416	Toddie Hinrich	zbisiker1o
1206138691	acobbledick1z@nationalgeographic.com	Edi Nock	+8106225688455	0472 Donald Crossing	5323 Pankratz Trail	Central Asia	3808 20621	Ade Cobbledick	sliger1p
1506315308	rreimer20@so-net.ne.jp	Yasmin D'Ugo	+5096720873572	63 Northfield Terrace	203 Swallow Street	Mayapada	7582 24518	Roy Reimer	sbavage1r
1406014012	jspeeks21@chicagotribune.com	Ruperto Hannigane	+6376124675842	7669 Londonderry Pass	867 Bunker Hill Center	Index Selindo	0705 97897	Julina Speeks	sguesford1t
1106485379	qcamosso22@soup.io	Dita Tansley	+3714479740464	31 Dixon Alley	9 Rigney Point	ICBC Indonesia	9103 40216	Quill Camosso	jluetkemeyers1u
1906913560	kmarritt23@vistaprint.com	Jillayne Scandrite	+1225905705480	82747 Claremont Trail	9368 Texas Place	Bank Rakyat Indonesia	5497 62243	Kay Marritt	mredon1v
1606121753	cflieg24@wufoo.com	Sutherland Scandrett	+7837613554093	460 Rieder Center	54650 Marcy Alley	Sinarmas	3026 50835	Celisse Flieg	nfairbrace1w
1406760488	lcrother25@miibeian.gov.cn	Nanci Cristofor	+1296131472621	36372 Oneill Court	16725 Lerdahl Park	DBS Indonesia	1885 85215	Leif Crother	korteaux1x
1906320852	ftoolan26@plala.or.jp	Budd Bilbrooke	+2252960597444	121 Hoepker Trail	51 David Terrace	Agroniaga	5238 92054	Fabiano Toolan	jfullagar1y
1006950485	tdalziell27@addtoany.com	Allianora Borleace	+4529478357095	5 Toban Drive	6288 Namekagon Avenue	DBS Indonesia	1700 33074	Thornton Dalziell	rruppele1z
1206191237	lsomerscales28@omniture.com	Pierrette Bernhardi	+4286751980945	44180 Kensington Junction	78 Iowa Way	Central Asia	9606 76426	Libbey Somerscales	hshapland20
1606306296	abatterbee29@nationalgeographic.com	Rustie Marwood	+4405661664624	00 Barby Parkway	91 Messerschmidt Road	Capital Indonesia	1220 61531	Alphonso Batterbee	dsabati21
1206574205	fmoye2a@cyberchimps.com	Wilden Margrett	+6296340204267	67 Thackeray Lane	6295 Continental Trail	Sinarmas	1074 94712	Felicia Moye	erenn22
1206524282	hkershow2b@blogger.com	Clarence Chesshire	+1726085229306	359 Reinke Point	493 Butterfield Lane	Mandiri	3288 93959	Hilary Kershow	kshailer23
1806626490	wdalgarnowch2c@harvard.edu	Prudi Antonnikov	+0345473316183	71045 Sage Court	09 Rusk Alley	Danamon	1887 10744	Wheeler Dalgarnowch	jyesipov24
1906429508	mbaxstar2d@blogtalkradio.com	Flossy Sells	+8159187712086	9165 Shelley Trail	1 Ramsey Road	DBS Indonesia	1610 17868	Myrtia Baxstar	fkeal25
1306218532	hpedron2e@cnbc.com	Demetri Farnsworth	+9090126105052	04391 Gerald Alley	2945 Meadow Ridge Alley	ICB Bumiputera	4345 70563	Hubert Pedron	datterley27
1906204883	smooney2f@shinystat.com	Jaymie Whitcher	+6842187712902	4398 Miller Hill	2 Loomis Alley	Bank Rakyat Indonesia	5756 20355	Sarina Mooney	nbegley29
1306307144	rorringe2g@blogspot.com	Dusty Baylay	+7815637528713	285 Summit Street	5980 Sheridan Parkway	Artha Graha Internasional	9245 28748	Richmond Orringe	tbaiyle2a
1406557975	ehallmark2h@github.io	Mortie Simms	+3734594983524	8302 Mcbride Court	26064 Farragut Street	Maspion	3965 92193	Ethe Hallmark	whinks2b
1306497428	dguyer2i@abc.net.au	Bank Harbottle	+4834716961257	00 Texas Plaza	8 Brentwood Place	BNP Paribas Indonesia	7340 93827	Derick Guyer	sallwood2c
1306179863	pbark2j@zimbio.com	Lucienne Golsworthy	+7056038715203	43547 Hauk Hill	019 Fieldstone Center	Commonwealth	3405 15066	Pammy Bark	sdockreay2d
1006104687	elate2k@ucla.edu	Sol Glenny	+9479932547915	841 Graceland Circle	98 Hanson Park	ICBC Indonesia	3038 40432	Elissa Late	nhonig2e
1806185670	halexandrescu2l@geocities.com	Drake Lesly	+5126233072113	36 Riverside Park	40426 Dayton Junction	Bumi Arta	7171 15631	Haley Alexandrescu	jforber2f
1206910208	nbeirne2m@sciencedirect.com	Kyle Birkenshaw	+3084385942757	58307 Elka Terrace	715 Dennis Plaza	Mega	6626 91139	Norman Beirne	jhorwell2g
1106472018	dklemmt2n@tiny.cc	Kristien Finlaison	+1426303945276	4947 Messerschmidt Pass	61814 Golden Leaf Center	SBI Indonesia	9036 94157	Dodie Klemmt	jsegges2h
1106611412	bmardoll2o@yale.edu	Stormy Biever	+3341326108347	04128 Meadow Ridge Place	879 Stephen Lane	Mandiri	2263 39630	Brnaba Mardoll	gokenfold2i
1506453445	kbackson2p@oaic.gov.au	Babara Dusey	+8734634011868	28 Oakridge Junction	37943 Fieldstone Way	Mandiri	3524 34830	Karol Backson	kwestney2j
1706193218	lholleran2q@sphinn.com	Asia Iacovaccio	+9955032458543	4 Rutledge Park	72 Kenwood Road	Permata	0255 21246	Layton Holleran	gphilbrook2k
1706854005	havraam2r@omniture.com	Davin Manuaud	+5530645253552	6224 Golden Leaf Junction	595 Heath Lane	Artha Graha Internasional	7532 96133	Hildegaard Avraam	rriddett2l
1706137145	lbartelot2s@japanpost.jp	Luisa Poznanski	+5291232814287	56895 5th Drive	8 Corscot Street	Metro Express	6199 68265	Laney Bartelot	eelcom2m
1206701835	ddowtry2t@netvibes.com	Stephan Cowup	+5433064060554	9523 Colorado Hill	06004 Orin Terrace	Danamon	4741 79099	Dana Dowtry	karmin2o
1406385872	sdavidovici2u@youtu.be	Mikaela Brydon	+5121594072027	27862 Utah Drive	38158 Lukken Center	Agroniaga	8165 90972	Shantee Davidovici	tmaceur2p
1206069390	kparkhouse2v@cornell.edu	Cathy Calvey	+1299577011061	36694 Little Fleur Plaza	58 Spenser Trail	BNP Paribas Indonesia	5981 70466	Kimberlyn Parkhouse	lzamora2q
1006033277	ccundict2w@tripod.com	Magdalene Stoyles	+6967260979589	5 Burning Wood Lane	5 Arapahoe Center	SBI Indonesia	6164 26061	Collette Cundict	pallder2r
1406788692	egallandre2x@meetup.com	Lesly Eustace	+0986535395097	8631 Forest Dale Center	77 Brickson Park Circle	Maybank	9319 25350	Ezra Gallandre	jpasterfield2s
1106769200	amcclancy2y@ox.ac.uk	Eric Grimbleby	+5260914468892	337 Northridge Road	48278 Toban Drive	Bank Negara Indonesia	9042 19117	Abraham McClancy	mdooler2t
1306435748	tmcleish2z@dion.ne.jp	Fancie Sawl	+0900415058622	4470 Golf Parkway	9681 Bartillon Junction	Mayapada	4367 98822	Teresina McLeish	rsaile2u
1606033596	agroundwator30@blogger.com	Karalynn Howden	+6768724332356	1488 Merchant Point	5630 Nevada Place	Bank Negara Indonesia	0833 53444	Austin Groundwator	nharker2w
1106990652	edrury31@cnet.com	Karole Antonsen	+6261413143462	6431 Vernon Parkway	7 Gulseth Avenue	Bank Rakyat Indonesia	9398 99277	Elora Drury	hshireff2x
1806226934	awoodison32@howstuffworks.com	Jeth Vannacci	+7390375130650	5 Memorial Trail	36477 Kedzie Way	BNP Paribas Indonesia	5307 59488	Agna Woodison	ppitkeathley2y
1806187981	ekiddey33@miibeian.gov.cn	Fayina Cartwight	+8988420539653	6423 Westport Plaza	8 Morrow Plaza	Maspion	3221 73266	Errick Kiddey	kdavidov2z
1006298741	pwoodley34@umn.edu	Donni Wessing	+5803556066615	8018 Rowland Junction	4711 Holmberg Street	Bukopin	3180 85460	Paulita Woodley	staffley30
1706414609	jburnitt35@earthlink.net	Scarlet Itzakson	+0235817785280	17511 Rockefeller Crossing	0 Forest Run Pass	BNP Paribas Indonesia	4121 23515	Jacquetta Burnitt	mbremen31
1306889437	cbaxter36@nba.com	Saxe Larkby	+4282121543795	51 Melvin Park	1 Knutson Hill	Danamon	2880 59824	Conny Baxter	pmustoo32
1206625430	scalkin37@scientificamerican.com	Cathee Alp	+5082777226618	45 Lakewood Gardens Way	610 Thompson Parkway	Bukopin	0830 91977	Sharleen Calkin	zstrutt33
1906149671	cbramford38@joomla.org	Tallie Enrique	+2868639522431	660 Scott Way	8 Namekagon Circle	SBI Indonesia	2827 86815	Catlin Bramford	kkubacki34
1606783983	skyffin39@google.fr	Michal Liversedge	+5855027488864	500 Hollow Ridge Road	910 Bellgrove Terrace	Mega	9635 96120	Shel Kyffin	rschusterl35
1006124360	erittmeyer3a@smh.com.au	Vinita Fawcett	+6550990205327	659 Oxford Street	870 Lotheville Park	Bank Negara Indonesia	6705 51615	Elmo Rittmeyer	tfendt36
1106590538	edusting3b@list-manage.com	Tess Soldan	+3576046047023	26003 Oneill Parkway	22 Shasta Trail	Bank Tabungan Negara	7889 90481	Elianora Dusting	ieyres37
1006663227	kivanshintsev3c@tripadvisor.com	Melba Deluca	+7924895463600	0342 Garrison Center	78243 Basil Road	Central Asia	2389 28776	Ki Ivanshintsev	hkiddell39
1506139378	cleynham3d@sciencedaily.com	Balduin Ellerington	+8852129449789	39021 Florence Alley	88317 Schmedeman Terrace	Barclays Indonesia	5217 21102	Cherey Leynham	mzapatero3a
1506030310	bsynder3e@histats.com	Mable Buffey	+5875381175915	82 Del Mar Junction	15849 Goodland Way	Capital Indonesia	4470 12171	Brig Synder	idaley3b
1306427173	pratnege3f@freewebs.com	Ase Boxhall	+6821700393125	4 Rowland Way	37508 Northridge Road	Maspion	7864 09404	Peta Ratnege	mtunkin3c
1806360513	tmacalester3g@cocolog-nifty.com	Upton Blundon	+6885951548111	862 Green Ridge Avenue	81 Menomonie Junction	SBI Indonesia	1845 16905	Ty MacAlester	mcolloff3d
1406667224	edantonio3h@tiny.cc	Linnea Yepiskov	+1396233812554	15 Sage Park	39 Green Park	Bumi Arta	4881 67316	Ezekiel D'Antonio	fgreensite3e
1406797996	mreddings3i@desdev.cn	Lora Duck	+5735749615896	087 Bluestem Road	73 Farmco Court	Permata	8266 92813	Merrie Reddings	iloveredge3f
1606846738	tdanbi3j@dailymail.co.uk	Tiphanie Every	+3870936928312	7 Petterle Hill	98 Knutson Point	Bumi Arta	7968 63995	Tawnya Danbi	ktoppin3g
1406029630	wdagwell3k@slate.com	Emmit Obin	+7884880134496	07435 Blackbird Junction	5615 Ryan Parkway	Danamon	5802 86994	Wilden Dagwell	dmillgate3h
1706824316	jarnot3l@amazon.co.uk	Erma Eckford	+0082724416391	1 Butternut Park	91536 Fairfield Place	Bank Negara Indonesia	5885 02651	Jo-anne Arnot	jmeldrum3j
1606935727	cpendrid3m@newyorker.com	Darlene Moller	+9519954338170	2184 Loftsgordon Parkway	66947 Maywood Trail	ICB Bumiputera	2504 56705	Carl Pendrid	mgristhwaite3k
1506372913	mguirau3n@webmd.com	Maura Pitkeathly	+0349426887510	64 Fair Oaks Drive	09 Hovde Court	Artha Graha Internasional	5521 65828	Madelyn Guirau	rbilton3l
1706145505	bizakovitz3o@google.cn	Sheila-kathryn La Batie	+4607833561526	89 Grayhawk Junction	284 Menomonie Lane	Mayapada	3262 17509	Brook Izakovitz	jbroxton3m
1506973518	bcumming3p@msu.edu	Sheffy Probart	+0012539572974	185 Basil Point	27 Thompson Street	Commonwealth	6410 02518	Becka Cumming	odell3n
1306877104	jcarriage3q@seesaa.net	Munroe Malecky	+2647167680850	26 Reinke Park	2801 Buell Park	Artha Graha Internasional	4971 74862	Josiah Carriage	rhemphall3o
1106428475	npresho3r@fda.gov	Franzen Brickwood	+5162228511308	81986 Onsgard Lane	70113 Knutson Parkway	Bank Tabungan Negara	4776 74089	Nicky Presho	msnewin3p
1106466242	rstairs3s@mysql.com	Patti Adolfson	+4575145847448	85221 Elgar Parkway	0 4th Drive	Barclays Indonesia	5875 20371	Ruggiero Stairs	taubry3q
1706372685	sledrun3t@stumbleupon.com	Ardelle Christophle	+4754479107568	495 Moose Drive	832 Sullivan Center	Mandiri	5722 56620	Steward Ledrun	garnli3r
1206114055	eratnege3u@ftc.gov	Dee dee Riseam	+5002327883955	3 Raven Road	3 Pearson Trail	Muamalat Indonesia	4090 53578	Edee Ratnege	dweildish3s
1006096383	bbrody3v@plala.or.jp	Goran Meriot	+8107697035513	5 Kensington Parkway	47 Kipling Place	Mega	3495 66341	Bald Brody	fhourihane3v
1306374420	sfleckney3w@narod.ru	Lizzie Gossage	+1122956745154	20 Shelley Junction	5093 6th Trail	DBS Indonesia	6838 47701	Shaun Fleckney	ldallas3w
1006792295	sgronaller3x@cam.ac.uk	Gerda Hardy-Piggin	+9745430632431	6958 Dwight Point	6418 Portage Parkway	Permata	9728 85337	Staci Gronaller	ddooland3x
1406390739	aschukert3y@plala.or.jp	Sari Cradey	+1371352537359	68422 Doe Crossing Avenue	9 Dennis Hill	Agroniaga	0896 02695	Arron Schukert	hradnage40
1106757668	nhillam3z@eventbrite.com	Kristi Necrews	+0728856952664	629 Algoma Point	1877 Everett Trail	BNP Paribas Indonesia	2892 29801	Norton Hillam	dcargen41
1606117895	dkelson40@dion.ne.jp	Alva Attlee	+5798599212301	01171 Crownhardt Place	4 Crowley Alley	Index Selindo	7282 26775	Demetra Kelson	qthowless42
1306323828	lvasilechko41@gravatar.com	Pincus Hackley	+9731192130946	515 Gulseth Park	45 Starling Drive	Bumi Arta	8265 90312	Lea Vasilechko	mwoodfield44
1206953084	htolmie42@discuz.net	Tracy Ditchett	+2460946043067	47890 Sunfield Hill	31856 West Plaza	Index Selindo	3819 41985	Hedda Tolmie	fschonfeld46
1606340207	rbwy43@acquirethisname.com	Noelani Folk	+1179865653044	50 Acker Street	480 Knutson Drive	Maybank	2411 97618	Rawley Bwy	hhambleton47
1806455055	tmcgaugan44@yolasite.com	Astrid Riddeough	+6316526232967	54131 Bunker Hill Plaza	2522 Little Fleur Plaza	Commonwealth	7075 70770	Tiffani McGaugan	jrodge48
1806547789	dmeere45@amazon.co.jp	Kizzie Gaughan	+9956416351515	8 Glendale Lane	08937 Grover Trail	Bumi Arta	6758 91323	Devin Meere	gboynton4a
1506366816	kcamerana46@mapy.cz	Daffi Racine	+9338685982365	2976 Caliangt Court	341 Prentice Court	ICB Bumiputera	2045 76729	Kliment Camerana	ewhitwam4b
1406922979	knoteyoung47@icio.us	Tiertza Goldstein	+3804364028934	3 Rusk Way	93549 Anthes Court	Agroniaga	8378 25087	Kirbie Noteyoung	jdumbare4c
1006976354	kgunning48@lulu.com	Cindi Millan	+8420763040786	5 Homewood Lane	008 Union Junction	Bank Negara Indonesia	3247 87968	Kristos Gunning	mmedcraft4e
1606985521	isteel49@gravatar.com	Claus Chipping	+2852074302153	2 Dapin Circle	92026 Carioca Plaza	Bank Negara Indonesia	9072 12929	Imelda Steel	gartus4f
1806538813	rgirardeau4a@ameblo.jp	Marline Tuther	+2833152822711	1567 Buell Crossing	0901 American Parkway	Mandiri	1616 14053	Rutter Girardeau	shodcroft4g
1406878965	rbraunstein4b@canalblog.com	Alain Agius	+8644846316582	606 Paget Parkway	99 Morning Road	Capital Indonesia	9912 05615	Riley Braunstein	ydilston4h
1906803604	ssicha4c@naver.com	Jackqueline Bodleigh	+5239644981711	60 Superior Way	135 Fremont Drive	ICBC Indonesia	8216 54396	Shandeigh Sicha	lroumier4i
1106602305	aohoolahan4d@is.gd	Jay Bargery	+2961533209492	46297 Warbler Pass	84 Beilfuss Parkway	Commonwealth	7780 67737	Asa O'Hoolahan	lvearncombe58
1006303784	bwelbelove4e@sphinn.com	Ollie Cable	+4527200267709	97511 Hovde Crossing	4 Waubesa Plaza	Bank Tabungan Negara	1923 10344	Blisse Welbelove	rhacard59
1106491081	kreichartz4f@histats.com	Howey Fawcitt	+6304473344284	84493 Holy Cross Junction	2514 Bluejay Drive	SBI Indonesia	5427 08313	Kermy Reichartz	rferrarese5a
1706721082	hdublin4g@discovery.com	Kassia Wrightson	+0708415647195	4385 South Way	8015 Summit Center	ICB Bumiputera	2861 00850	Hermie Dublin	sarens5b
1706125680	mbettaney4h@cnn.com	Cara Syer	+2619294882301	9 Roxbury Road	4 Cascade Parkway	Bank Rakyat Indonesia	1933 20506	Marcello Bettaney	vbrosnan5c
1606297213	lwingfield4i@samsung.com	Marne Dronsfield	+4837029399886	6917 Stuart Crossing	30 Superior Alley	Mestika Dharma	7765 48011	Lilla Wingfield	jvedntyev5g
1306512469	areneke4j@nih.gov	Kelci Caslett	+2056999169952	59024 Hollow Ridge Point	81639 Heath Court	Bank Rakyat Indonesia	4483 30034	Aguste Reneke	rstanden5h
1306477028	aduny4k@loc.gov	Adore Mawer	+0852202765155	99 Orin Way	8392 Mosinee Pass	Permata	5458 31386	Abigael Duny	lleppingwell5i
1406197577	ccragg4l@paginegialle.it	Lon Flacknell	+1264881780298	27 Ronald Regan Street	6 Helena Drive	Barclays Indonesia	8602 88836	Clarice Cragg	gmorison5j
1206365324	rmougeot4m@google.pl	Rosemonde Rubens	+0313424400824	87 Little Fleur Way	6855 Sullivan Court	Sinarmas	9652 96908	Rex Mougeot	kshory5k
1406566237	mveld4n@tamu.edu	Drew Zealey	+8408918763951	529 Straubel Hill	36 Harper Park	Index Selindo	5843 09686	Marlowe Veld	wcookney5l
1206059263	bmarchington4o@unblog.fr	Raven Surtees	+3597498998940	9050 Lillian Avenue	87 Mallard Center	ICB Bumiputera	9178 79206	Bondie Marchington	gkorf5m
1606562674	tswale4p@hibu.com	Yulma Kentwell	+6073616307006	33 Rusk Alley	17 Sutteridge Trail	OCBC NISP	8953 35363	Tito Swale	sashworth5n
1806195446	eschoenrock4q@slate.com	Ernesto Fridaye	+8007205043611	7692 David Plaza	8081 Hallows Drive	Maspion	8516 21542	Elbertine Schoenrock	ewortley5p
1106022312	tstapleton4r@accuweather.com	Angela Martland	+6966090556109	63202 Shopko Place	8641 Village Green Alley	Mayapada	2648 09504	Terrie Stapleton	vjikylls5q
1306446209	meixenberger4s@i2i.jp	Stillman Neild	+9086546326472	7768 Melody Junction	96 Coolidge Point	Index Selindo	8753 13491	Michell Eixenberger	mclaricoates5s
1906633707	ofursse4t@yale.edu	Enrichetta MacInherney	+1183829491453	30 Emmet Way	98405 Chive Way	Maybank	7276 92079	Orion Fursse	rbrafield5t
1706820031	dchaimson4u@statcounter.com	Malissa Gershom	+3313510936172	4519 Mitchell Point	190 Harbort Road	Maspion	5067 60409	Devlin Chaimson	ltheze5u
1306240346	tsergean4v@ted.com	Joyous Vannini	+0818025976287	530 Del Mar Parkway	5514 Northridge Alley	Central Asia	4770 66090	Tomasine Sergean	lcheng5v
1806625928	vallan4w@seesaa.net	Van Mahedy	+0985545031950	1 Service Parkway	46832 Canary Court	Mayapada	7568 11431	Velvet Allan	swinsome5y
1606096447	csell4x@phpbb.com	Alejandro Reddoch	+6207993130960	7209 Lien Pass	40523 Moland Trail	Maybank	6246 95020	Chiarra Sell	swildsmith5z
1406205713	tcaff4y@nydailynews.com	Katherina Resun	+9325696360073	07 Sloan Place	0 Johnson Road	Maybank	9589 93354	Teressa Caff	mmeadows62
1006889349	rsellan4z@sakura.ne.jp	Ulrika Tissell	+1036337837989	48261 Old Shore Park	75889 Welch Crossing	CIMB Niaga	3040 04263	Riobard Sellan	ukrysztowczyk63
1606006647	bpetcher50@admin.ch	Moritz Starling	+4841392872109	6 Novick Alley	249 Holmberg Point	Bukopin	8573 34052	Biron Petcher	gsmaleman64
1306801343	clintall51@soundcloud.com	Rudolfo Cullingworth	+2714476970708	66214 Lindbergh Plaza	48432 8th Center	Muamalat Indonesia	5398 05001	Cristian Lintall	jlindbergh65
1706727511	rlongmuir52@guardian.co.uk	Sunny Mort	+7987782070762	29 Clarendon Junction	5 Eggendart Trail	DBS Indonesia	0265 07789	Roanna Longmuir	efurminger67
1706501010	bgrevel53@studiopress.com	Nial Jantel	+7811408001386	200 Lawn Parkway	03052 Butterfield Drive	Mega	9170 87520	Bernadine Grevel	dcoventry68
1606430627	ajewes54@i2i.jp	Helena MacNeilly	+9830866653733	738 Mayfield Park	93040 Dakota Street	Barclays Indonesia	8427 23953	Ardys Jewes	fwhitmore69
1906588075	epiddlesden55@shop-pro.jp	Beau Vass	+1801812836032	49173 Summer Ridge Circle	502 Dovetail Park	ICB Bumiputera	0782 23303	Elvis Piddlesden	ghulatt6a
1406605255	qcrickett56@disqus.com	Silvester Gellately	+3002502901432	1003 Judy Plaza	787 Schiller Court	Permata	5076 21411	Quinn Crickett	esholl6b
1606687187	hmerriton57@friendfeed.com	Laurette Thecham	+9969996029478	54717 Brickson Park Circle	05589 Fair Oaks Center	Danamon	3618 18618	Hillel Merriton	gsoule6c
1206365466	hdorber58@chronoengine.com	Ana Forbear	+1533014460028	471 Gateway Alley	2 Badeau Parkway	Barclays Indonesia	8867 15107	Hanson Dorber	jpeschka6d
1106603614	salster59@bandcamp.com	Violante Makin	+8916188996622	29 6th Alley	6 Arapahoe Terrace	Sinarmas	0705 42254	Shaun Alster	iivshin6e
1506247620	ccosford5a@drupal.org	Shalom Nabbs	+1193320813607	397 Lillian Street	4 Sunbrook Street	Index Selindo	7345 09075	Carlos Cosford	ishepheard6f
1706431770	kchree5b@cbslocal.com	Christa L' Estrange	+3175201417065	5098 Miller Pass	8 Milwaukee Crossing	Maybank	0267 00744	Kim Chree	erubinowicz6h
1106662519	ogodlee5c@lycos.com	Avril Ollivier	+1606605561840	98 Toban Park	57 Carey Street	Metro Express	8837 62219	Odie Godlee	tpllu6i
1306958575	bkirimaa5d@hc360.com	Jammal Davioud	+6152670489399	46874 Claremont Terrace	6 Monterey Avenue	Commonwealth	1892 96756	Betta Kirimaa	jdilland6j
1906084911	emc5e@dropbox.com	Elinor Clacey	+1518925541386	79 New Castle Place	426 Prairie Rose Lane	ICBC Indonesia	3963 07644	Eberto Mc Queen	fketchen6k
1806874498	horrin5f@mtv.com	Kathye Bullant	+5322275916344	53 Montana Parkway	2 Tony Crossing	Kesawan	8545 26596	Hewe Orrin	kcorkitt6l
1606096981	mvelez5g@npr.org	Keen Easdon	+8898241746717	332 Canary Parkway	60195 Oak Valley Park	Mandiri	4931 90526	Mikael Velez	hmahony6m
1106989013	aicom5h@google.ru	Ysabel Kenzie	+6986207993314	92 Killdeer Point	226 Kennedy Drive	Danamon	3824 49596	Ashlie Icom	cortells6n
1406481802	ncopnall5i@domainmarket.com	Arlyne Bagley	+2644129763998	60 Dexter Hill	0725 Forest Run Parkway	ICBC Indonesia	7447 07398	Norris Copnall	cmcduffy6o
1206900812	tspancock5j@seesaa.net	Frannie Ovise	+5321914626975	3635 Fair Oaks Plaza	5 Vahlen Drive	CIMB Niaga	0017 07014	Tabby Spancock	hsedgwick6p
\.


--
-- Data for Name: pembayaran; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY pembayaran (urutan, kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm, keterangan, tgl_bayar, nominal) FROM stdin;
100	33199	100	1706854005	eu	2017-09-07	63
1	99091	1	1306133042	vitae	2017-07-29	83
2	84840	2	1806827299	a	2018-04-01	52
3	16235	3	1706374528	magnis	2018-03-06	28
4	37889	4	1506994587	duis	2018-04-08	59
5	49570	5	1306413101	dictumst	2018-02-19	59
6	72067	6	1406425546	leo	2018-04-14	99
7	56652	7	1306254708	sociis	2017-10-21	86
8	26459	8	1706443462	donec	2017-09-05	81
9	81397	9	1306151865	nulla	2017-06-01	30
10	35819	10	1206420944	congue	2018-02-09	5
11	62235	11	1806318404	ut	2017-10-14	40
12	10089	12	1706358415	mauris	2018-01-17	48
13	39750	13	1206240645	orci	2017-08-14	94
14	15842	14	1206526391	cursus	2018-01-17	18
15	4942	15	1206301664	fusce	2017-08-06	16
16	79051	16	1606362154	magna	2017-07-10	65
17	21793	17	1106280943	ac	2017-07-01	91
18	33359	18	1106115142	nec	2017-12-09	39
19	91678	19	1506452868	non	2018-02-14	26
20	91748	20	1806624411	enim	2018-03-06	17
21	94848	21	1006871284	id	2017-12-06	36
22	21608	22	1306169650	cursus	2018-04-06	89
23	25797	23	1906900502	nec	2018-03-11	84
24	53893	24	1606847536	nulla	2017-07-03	88
25	59564	25	1006447470	magna	2017-11-11	65
26	90191	26	1206296516	amet	2017-05-30	78
27	23763	27	1006340824	enim	2018-01-05	51
28	80238	28	1506238587	sapien	2018-01-19	48
29	38326	29	1406203323	vulputate	2018-03-09	31
30	42535	30	1606998965	leo	2017-11-19	30
31	4591	31	1906245803	vitae	2017-09-16	24
32	48686	32	1406452300	eget	2018-02-12	84
33	65276	33	1806025248	suspendisse	2017-10-18	80
34	9291	34	1406802924	ut	2018-02-04	70
35	578	35	1106941116	dui	2017-05-10	94
36	10009	36	1806273566	rhoncus	2018-02-09	65
37	53938	37	1106566949	quam	2017-12-02	74
38	47339	38	1906883395	phasellus	2017-08-04	64
39	27651	39	1006124179	in	2018-01-24	32
40	48445	40	1806974954	ante	2017-08-26	18
41	79085	41	1606189532	aenean	2017-08-25	95
42	19546	42	1706002472	vel	2018-02-07	69
43	37058	43	1606166321	nam	2017-07-22	41
44	29760	44	1206914821	nisi	2017-07-12	46
45	20890	45	1906347959	quis	2018-02-18	55
46	43942	46	1806390205	duis	2017-10-25	16
47	92838	47	1106725303	risus	2018-01-07	84
48	8041	48	1806239573	donec	2018-02-01	79
49	61320	49	1606517541	habitasse	2017-07-12	47
50	33199	50	1406633378	mi	2018-04-05	19
51	99091	51	1706698635	diam	2017-08-26	76
52	84840	52	1306238740	aenean	2018-01-29	71
53	16235	53	1106405560	tortor	2017-08-30	39
54	37889	54	1506041623	est	2017-09-15	49
55	49570	55	1206467185	sit	2018-04-08	75
56	72067	56	1406838692	luctus	2018-03-12	52
57	56652	57	1906237256	cum	2017-07-05	5
58	26459	58	1706726175	nec	2017-07-24	36
59	81397	59	1506551420	diam	2017-07-06	8
60	35819	60	1706452587	suscipit	2017-10-11	28
61	62235	61	1206265578	non	2018-03-04	32
62	10089	62	1906772612	ligula	2017-11-04	91
63	39750	63	1706861852	suscipit	2017-12-20	68
64	15842	64	1106824845	ante	2017-12-23	100
65	4942	65	1206762117	commodo	2017-11-06	56
66	79051	66	1306476813	lobortis	2018-02-07	28
67	21793	67	1706603949	porta	2017-05-14	73
68	33359	68	1406227772	id	2017-11-18	56
69	91678	69	1506479098	rutrum	2017-08-28	45
70	91748	70	1206781746	aliquam	2017-11-17	22
71	94848	71	1006671469	neque	2018-02-25	13
72	21608	72	1206138691	mi	2018-03-08	58
73	25797	73	1506315308	morbi	2017-06-16	4
74	53893	74	1406014012	diam	2018-04-28	50
75	59564	75	1106485379	et	2017-09-05	59
76	90191	76	1906913560	convallis	2017-07-11	50
77	23763	77	1606121753	ipsum	2017-06-01	86
78	80238	78	1406760488	varius	2017-11-21	59
79	38326	79	1906320852	id	2017-11-15	22
80	42535	80	1006950485	felis	2017-12-04	25
81	4591	81	1206191237	sit	2018-01-23	19
82	48686	82	1606306296	tempus	2017-06-16	19
83	65276	83	1206574205	eu	2018-03-25	79
84	9291	84	1206524282	dolor	2018-04-26	23
85	578	85	1806626490	id	2017-05-20	78
86	10009	86	1906429508	nulla	2017-07-29	83
87	53938	87	1306218532	in	2017-11-12	96
88	47339	88	1906204883	nisi	2017-08-05	88
89	27651	89	1306307144	nec	2017-07-19	69
90	48445	90	1406557975	sapien	2017-07-29	67
91	79085	91	1306497428	sapien	2017-12-14	72
92	19546	92	1306179863	nulla	2018-01-23	59
93	37058	93	1006104687	quam	2018-03-25	33
94	29760	94	1806185670	parturient	2017-06-13	3
95	20890	95	1206910208	vestibulum	2018-04-26	4
96	43942	96	1106472018	nunc	2018-02-20	71
97	92838	97	1106611412	nisl	2017-09-28	28
98	8041	98	1506453445	est	2017-12-29	41
99	61320	99	1706193218	convallis	2017-06-10	54
\.


--
-- Data for Name: pendaftaran; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY pendaftaran (no_urut, kode_skema_beasiswa, npm, waktu_daftar, status_daftar, status_terima) FROM stdin;
1	99091	1306133042	2017-12-03 00:00:00	true	false
2	84840	1806827299	2017-11-23 00:00:00	false	true
3	16235	1706374528	2017-09-07 00:00:00	false	true
4	37889	1506994587	2017-11-15 00:00:00	false	false
5	49570	1306413101	2017-10-01 00:00:00	false	true
6	72067	1406425546	2017-08-03 00:00:00	true	false
7	56652	1306254708	2018-01-18 00:00:00	true	true
8	26459	1706443462	2018-04-09 00:00:00	false	true
9	81397	1306151865	2017-12-04 00:00:00	true	false
10	35819	1206420944	2017-04-30 00:00:00	true	false
11	62235	1806318404	2018-02-20 00:00:00	false	false
12	10089	1706358415	2017-07-13 00:00:00	true	true
13	39750	1206240645	2018-01-31 00:00:00	false	false
14	15842	1206526391	2017-11-24 00:00:00	true	true
15	4942	1206301664	2018-03-14 00:00:00	true	false
16	79051	1606362154	2017-06-03 00:00:00	false	false
17	21793	1106280943	2017-07-07 00:00:00	false	false
18	33359	1106115142	2017-07-31 00:00:00	false	true
19	91678	1506452868	2017-06-04 00:00:00	true	false
20	91748	1806624411	2018-01-18 00:00:00	false	true
21	94848	1006871284	2017-07-29 00:00:00	false	false
22	21608	1306169650	2017-05-15 00:00:00	false	true
23	25797	1906900502	2017-09-10 00:00:00	false	true
24	53893	1606847536	2017-11-16 00:00:00	false	false
25	59564	1006447470	2017-08-20 00:00:00	true	true
26	90191	1206296516	2017-11-27 00:00:00	true	true
27	23763	1006340824	2018-03-10 00:00:00	false	false
28	80238	1506238587	2018-01-24 00:00:00	true	true
29	38326	1406203323	2017-09-23 00:00:00	false	true
30	42535	1606998965	2018-04-19 00:00:00	false	true
31	4591	1906245803	2018-04-07 00:00:00	false	true
32	48686	1406452300	2017-11-05 00:00:00	true	false
33	65276	1806025248	2018-03-10 00:00:00	true	true
34	9291	1406802924	2017-11-03 00:00:00	false	false
35	578	1106941116	2018-02-18 00:00:00	false	false
36	10009	1806273566	2017-08-27 00:00:00	true	true
37	53938	1106566949	2017-11-26 00:00:00	true	true
38	47339	1906883395	2018-03-24 00:00:00	false	false
39	27651	1006124179	2017-11-29 00:00:00	true	false
40	48445	1806974954	2017-11-01 00:00:00	true	false
41	79085	1606189532	2017-12-23 00:00:00	true	false
42	19546	1706002472	2018-02-24 00:00:00	false	true
43	37058	1606166321	2017-11-20 00:00:00	true	true
44	29760	1206914821	2017-11-03 00:00:00	false	true
45	20890	1906347959	2017-10-16 00:00:00	false	false
46	43942	1806390205	2017-09-16 00:00:00	true	true
47	92838	1106725303	2018-01-24 00:00:00	true	false
48	8041	1806239573	2017-11-22 00:00:00	true	true
49	61320	1606517541	2017-07-19 00:00:00	false	false
50	33199	1406633378	2017-11-16 00:00:00	false	true
51	99091	1706698635	2017-06-07 00:00:00	false	true
52	84840	1306238740	2018-04-07 00:00:00	true	true
53	16235	1106405560	2017-10-09 00:00:00	true	false
54	37889	1506041623	2017-10-17 00:00:00	true	false
55	49570	1206467185	2017-05-07 00:00:00	false	true
56	72067	1406838692	2017-08-24 00:00:00	true	false
57	56652	1906237256	2017-07-26 00:00:00	true	false
58	26459	1706726175	2017-07-31 00:00:00	false	false
59	81397	1506551420	2017-07-30 00:00:00	false	true
60	35819	1706452587	2018-01-15 00:00:00	false	true
61	62235	1206265578	2018-02-12 00:00:00	false	true
62	10089	1906772612	2018-01-24 00:00:00	true	false
63	39750	1706861852	2017-08-03 00:00:00	false	false
64	15842	1106824845	2017-11-05 00:00:00	false	true
65	4942	1206762117	2017-07-20 00:00:00	true	false
66	79051	1306476813	2017-05-06 00:00:00	false	true
67	21793	1706603949	2017-12-17 00:00:00	false	true
68	33359	1406227772	2017-06-21 00:00:00	true	true
69	91678	1506479098	2017-05-25 00:00:00	true	true
70	91748	1206781746	2017-07-19 00:00:00	false	false
71	94848	1006671469	2017-09-08 00:00:00	true	false
72	21608	1206138691	2017-10-07 00:00:00	true	true
73	25797	1506315308	2018-02-12 00:00:00	true	false
74	53893	1406014012	2017-08-30 00:00:00	false	false
75	59564	1106485379	2017-07-06 00:00:00	true	false
76	90191	1906913560	2017-09-10 00:00:00	true	true
77	23763	1606121753	2018-04-01 00:00:00	false	true
78	80238	1406760488	2017-08-18 00:00:00	true	true
79	38326	1906320852	2018-01-23 00:00:00	false	false
80	42535	1006950485	2017-12-22 00:00:00	false	false
81	4591	1206191237	2017-11-08 00:00:00	false	false
82	48686	1606306296	2018-03-26 00:00:00	false	true
83	65276	1206574205	2018-03-02 00:00:00	false	true
84	9291	1206524282	2017-10-01 00:00:00	false	false
85	578	1806626490	2018-02-17 00:00:00	true	true
86	10009	1906429508	2017-10-25 00:00:00	false	true
87	53938	1306218532	2018-02-05 00:00:00	false	false
88	47339	1906204883	2018-04-10 00:00:00	true	false
89	27651	1306307144	2017-11-21 00:00:00	true	true
90	48445	1406557975	2017-08-15 00:00:00	true	false
91	79085	1306497428	2018-01-17 00:00:00	true	true
92	19546	1306179863	2017-09-01 00:00:00	false	true
93	37058	1006104687	2017-10-26 00:00:00	false	true
94	29760	1806185670	2017-09-28 00:00:00	true	true
95	20890	1206910208	2017-11-22 00:00:00	true	true
96	43942	1106472018	2017-11-07 00:00:00	true	false
97	92838	1106611412	2018-01-07 00:00:00	true	false
98	8041	1506453445	2017-05-29 00:00:00	true	true
99	61320	1706193218	2017-06-29 00:00:00	false	true
100	33199	1706854005	2018-03-23 00:00:00	false	false
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY pengguna (username, password, role) FROM stdin;
alambrookn	NY20DxsNuJuw	donatur
azambont	1WxDHi5J2eXJ	donatur
blindbergv	QI7T0FoMBhg4	donatur
akohrty	CeEYUc2	donatur
dmilkin5	KHigfG4	mahasiswa
sroskeilly6	M6sfYi3	mahasiswa
echantrell7	8GT5Ftw4g	mahasiswa
mlorkins8	mWuhrA4Oi	mahasiswa
ypilley9	WIcxk7mso	mahasiswa
gslingera	pyD1XvS5nLx5	mahasiswa
ghubaneb	Hia4kK4vRD06	mahasiswa
nrotherac	3DZU4swtT	mahasiswa
wonionsd	f4qa2thN	mahasiswa
sbraistede	XYTbHbPvJMz	mahasiswa
mmaffyf	9ZnIIzc8GUk	mahasiswa
cweighg	9tLiSA4	mahasiswa
dwindrassh	d7zwCFaWqy	mahasiswa
mpolleyi	STC7AEvpx	mahasiswa
pmeadwayj	aMT0fk1W	mahasiswa
mfarendenk	MZfXTK	mahasiswa
jjohananoffl	fUWxuh	mahasiswa
tbenettolo0	1lMm2a9cddq	admin
dnowlan1	VQf7FH	admin
kgirdwood2	7CRrHGKk2oG	admin
mmcinnerny3	5POzikcGPU	admin
mjanoch4	H7p0CdN	admin
bcooley16	1PSejoT2Y095	donatur
bburkett1e	1YQ9vOpt	donatur
cgarwood1q	XTGu3hgvYyP	donatur
cbilton1s	SO7H9gkgK7	donatur
bholmyard26	9v9TZragl	donatur
araoult28	XK319Tr23y8	donatur
bfransinelli2n	QJQnW8	donatur
blavers2v	luWwwbQpAK	donatur
cgilleon38	FIz5ZI	donatur
bhallward3i	vkhYNe	donatur
cbromet3t	qIHRMj2	donatur
bberrick3u	digPVFTjbM	donatur
cmatyashev3y	rjhfhA	donatur
abanbrigge3z	k3kCvIjN7MK	donatur
chenker43	SCPuM5TA	donatur
bcardiff45	Wzryx6	donatur
cjepson49	HYaYdYLq7O	donatur
ddybald4j	X1g8Vhgk	mahasiswa
ttschersich4k	XptVj6Pfi	mahasiswa
mcreamen4l	jOb8gtkpwZXl	mahasiswa
gbrockton4n	7Y57SfBo	mahasiswa
durch4o	5r0JLAUYYRDm	mahasiswa
lroberts4p	tHL9ZZgV153	mahasiswa
pgodly4q	vTsz5Hlr3V4	mahasiswa
fcremer4r	KypFgwakISE	mahasiswa
lsteketee4s	eIqzBceAAVGW	mahasiswa
jfirmage4t	V7VKMYW	mahasiswa
crapelli4u	1ea7qKj	mahasiswa
ldeviney4v	kD9ljJ1vf	mahasiswa
dbordessa4w	S1V3yYXm0j	mahasiswa
mdubery4x	QvnjYD	mahasiswa
ptills4y	Ymbla5RNmd	mahasiswa
mspender4z	SQisJLQV	mahasiswa
mfranceschi51	FlMzQKWHok7	mahasiswa
tblamphin54	x1AzQkjShgY	mahasiswa
kcornejo55	332ODVDo14	mahasiswa
draymond56	KVJJIzS6a	mahasiswa
zglaum57	vcrEbyL8K	mahasiswa
avacher4d	jgmEhlF0r	donatur
cflaubert4m	yfh8yE	donatur
arizzi50	MLoSKfp	donatur
cchisholm52	1IqL8A7wi	donatur
aomrod53	Tpfow8MOBsD	donatur
bbracknell5d	ED0DdtUXX	donatur
ajoanic5e	HfWg9FbvjdE	donatur
amaydway5f	6OrRFkt	donatur
bmcgeever5o	YitiZVu	donatur
agallie5r	7RXMaKUy	donatur
atocher5w	NMgljMn	donatur
aratray5x	mEqRRFls78W	donatur
cboden60	KTB6UY8	donatur
cdurrad61	mt28qyxfhP5	donatur
adurrand66	j7VrarF	donatur
athompson6g	F2LC9d	donatur
aheller6s	Yxj69QahPf	donatur
awalcar6v	vbDXS661k6	donatur
blidgertwood6y	TZXMiP	donatur
bbarracks70	ZW3SC8S3NL	donatur
afrift77	Q6A7XNP1C	donatur
cmathevet7d	KEmpDeXg56fI	donatur
cludovici7p	LRPzNQN	donatur
cgirauld7r	jExaTPzqTTGP	donatur
bcakes7s	UYpin0plHW	donatur
aferrao7t	8F0y7TJd	donatur
asodeau7w	4i3OvBAG	donatur
cmccollum7z	l882ErWd	donatur
bfergusson89	iocF6d	donatur
qmeecherm	9ny4NJQY08	mahasiswa
zthickinso	ihdIa8n4r	mahasiswa
jmichelinp	hIROZrG10	mahasiswa
jduerdenq	FgwVpyUMv	mahasiswa
jwellanr	RuCBrXV9ub	mahasiswa
msmithens	jFroilw	mahasiswa
shuygheu	AneHa8zR	mahasiswa
pbourchierw	KjqyocKG1WC0	mahasiswa
pspearex	UyZ3O7pxG	mahasiswa
maishz	1Vs0JM	mahasiswa
jcamilletti10	Q2DzIreMV	mahasiswa
sdesavery11	KyXYSOlsnoy	mahasiswa
hmaclise12	EcbPAyr8	mahasiswa
jcaine13	3vDRlv5x8fui	mahasiswa
mpauling14	oT7sheBU9	mahasiswa
wstopper15	QLx1DWCW	mahasiswa
wchazelle17	zX9ZIN	mahasiswa
tsneezum18	iKIEwgi8Y	mahasiswa
jhallmark19	Jd7cR9dhBCvx	mahasiswa
fbooton1a	XFpIYY7uK	mahasiswa
sbowie1b	YwrUWfhD	mahasiswa
dkenwell1c	Z9lGP5v	mahasiswa
toldroyde1d	gjg1Zcxqug	mahasiswa
dmacshane1f	SrNS34Xlkxk	mahasiswa
lgawne1g	aajUPp	mahasiswa
mboddymead1h	8zj8zPPRq	mahasiswa
htwiddy1i	bR6pa7	mahasiswa
wteek1j	D8OsVG2Gi	mahasiswa
mwarboy1k	sLXav6sq	mahasiswa
wmcgifford1l	mfszCoZt	mahasiswa
gsyphas1m	h3Itcq	mahasiswa
jcolebourne1n	YERaAKIm	mahasiswa
zbisiker1o	bsrsFxs	mahasiswa
sliger1p	XTRkUe9wkFD	mahasiswa
sbavage1r	2zS5YrTE	mahasiswa
sguesford1t	ywpUOOY9EA	mahasiswa
jluetkemeyers1u	ldztRwxs7	mahasiswa
mredon1v	TXAAOq5	mahasiswa
nfairbrace1w	4wXQhQ	mahasiswa
korteaux1x	bR6QR0y5TBi	mahasiswa
jfullagar1y	rpbQDAud	mahasiswa
rruppele1z	5KDIz9N	mahasiswa
hshapland20	HI840L95	mahasiswa
dsabati21	UqzzddJNP	mahasiswa
erenn22	wtAant4oVw	mahasiswa
kshailer23	esZTVlN8o5k	mahasiswa
jyesipov24	COOBth9Yijn	mahasiswa
fkeal25	qJhsc6G6N9X	mahasiswa
datterley27	rlP7sFRoWk4	mahasiswa
nbegley29	3aOEx2	mahasiswa
tbaiyle2a	mjfePN	mahasiswa
whinks2b	fpxzerJrz	mahasiswa
sallwood2c	QasKUlHqKl	mahasiswa
sdockreay2d	nkUBUO	mahasiswa
nhonig2e	wdwwwHu7cr	mahasiswa
jforber2f	R0BXIOS0Z	mahasiswa
jhorwell2g	gy1BKKF	mahasiswa
jsegges2h	XCOhW21	mahasiswa
gokenfold2i	IkyunyiqVihK	mahasiswa
kwestney2j	IIKDhbl	mahasiswa
gphilbrook2k	bE9aHpR	mahasiswa
rriddett2l	Ocxi5jP1f	mahasiswa
eelcom2m	AYSEqb	mahasiswa
karmin2o	cj923oLYVd	mahasiswa
tmaceur2p	XXeFiRLnf	mahasiswa
lzamora2q	mEXIKNRz	mahasiswa
pallder2r	lR9iMsDGjgVD	mahasiswa
jpasterfield2s	lFZoeyWQ7HO	mahasiswa
mdooler2t	zbn0outPpR90	mahasiswa
rsaile2u	muB826lhfUr	mahasiswa
nharker2w	ntQh83kc	mahasiswa
hshireff2x	i1A1PRhHq	mahasiswa
ppitkeathley2y	AWAgezKKy	mahasiswa
kdavidov2z	yoW9fwhUKk	mahasiswa
staffley30	xqeT4RaqzM	mahasiswa
mbremen31	89DhsLy4	mahasiswa
pmustoo32	TzbSaHVvD	mahasiswa
zstrutt33	eCXuoCZfpK0W	mahasiswa
kkubacki34	NirfZye	mahasiswa
rschusterl35	nVJboWfFrc	mahasiswa
tfendt36	NMKrqMyg6	mahasiswa
ieyres37	IJe9ynLq	mahasiswa
hkiddell39	4DHkfwisBws	mahasiswa
mzapatero3a	C26FDS7	mahasiswa
idaley3b	XQqT5qwcU	mahasiswa
mtunkin3c	TEDLhoWVqm	mahasiswa
mcolloff3d	zTbz7mRHv	mahasiswa
fgreensite3e	PANNUyj	mahasiswa
iloveredge3f	ncNn30gv	mahasiswa
ktoppin3g	jReeeoMjt	mahasiswa
dmillgate3h	oGPc3h	mahasiswa
jmeldrum3j	ciwFRgZ9qt	mahasiswa
mgristhwaite3k	ynQ5pF5Z	mahasiswa
rbilton3l	eTqLiN	mahasiswa
jbroxton3m	1uGIMNIN	mahasiswa
odell3n	c3zGWqKJSies	mahasiswa
rhemphall3o	FALURciZF	mahasiswa
msnewin3p	KPyiDYR0A7u	mahasiswa
taubry3q	XboXZXE	mahasiswa
garnli3r	NUXkX4jckNuf	mahasiswa
dweildish3s	fCnDxID8GQBU	mahasiswa
fhourihane3v	zFEqkR	mahasiswa
ldallas3w	uCxICrEi	mahasiswa
ddooland3x	Htj2g7cIS	mahasiswa
hradnage40	SD6Jdi7rWv	mahasiswa
dcargen41	lDu8Vt4	mahasiswa
qthowless42	Rt5aTYlDq	mahasiswa
mwoodfield44	qXIfoI3lK02	mahasiswa
fschonfeld46	30mUNik5Y	mahasiswa
hhambleton47	8kLUrM	mahasiswa
jrodge48	uFvUG6gYo	mahasiswa
gboynton4a	CDPSIfAZL	mahasiswa
ewhitwam4b	YaFdqyZlrDr9	mahasiswa
jdumbare4c	9BtrvPniskPR	mahasiswa
mmedcraft4e	EWySJKcoA	mahasiswa
gartus4f	ccG8Ny	mahasiswa
shodcroft4g	LPsKrf	mahasiswa
ydilston4h	1Bv65BWaND	mahasiswa
lroumier4i	wiQ6Yzr	mahasiswa
lvearncombe58	krR30QCY	mahasiswa
rhacard59	R3ld8o	mahasiswa
rferrarese5a	K9qlybA76	mahasiswa
sarens5b	bICl89n	mahasiswa
vbrosnan5c	bm4eGEmc9	mahasiswa
jvedntyev5g	GHCKwKvlr	mahasiswa
rstanden5h	pEBF3Fj6c	mahasiswa
lleppingwell5i	hLYMcEI	mahasiswa
gmorison5j	Mxw0ZIRi0g9k	mahasiswa
kshory5k	Bbja1oN	mahasiswa
wcookney5l	tLrbe3xwL	mahasiswa
gkorf5m	59f0dY	mahasiswa
sashworth5n	EeyauiWp	mahasiswa
ewortley5p	44rBLW	mahasiswa
vjikylls5q	Mg6dR8c1	mahasiswa
mclaricoates5s	f9s9sEDqp	mahasiswa
rbrafield5t	dnD8n1KM	mahasiswa
ltheze5u	yK8Wjcmmn	mahasiswa
lcheng5v	JFago3	mahasiswa
swinsome5y	oSIbZIow	mahasiswa
swildsmith5z	zYhL8wi	mahasiswa
mmeadows62	o7czeePpU6	mahasiswa
ukrysztowczyk63	G4PXkesGcuRr	mahasiswa
gsmaleman64	8wZEpd39	mahasiswa
jlindbergh65	2X7emALSrRk	mahasiswa
efurminger67	FxPQy8YDiR	mahasiswa
dcoventry68	CUCSMcoNt4u	mahasiswa
fwhitmore69	lkf1uw	mahasiswa
ghulatt6a	ZO6b6ax2j	mahasiswa
esholl6b	y6RZdbtX	mahasiswa
gsoule6c	ubYkfg	mahasiswa
jpeschka6d	JYM7h2	mahasiswa
iivshin6e	dWE29rlw	mahasiswa
ishepheard6f	pIKXrNiDX	mahasiswa
erubinowicz6h	WQtVNFV	mahasiswa
tpllu6i	Equ5k16	mahasiswa
jdilland6j	Rw5ylRpRv8	mahasiswa
fketchen6k	Eu0mJdOc	mahasiswa
kcorkitt6l	U19s2yX4N	mahasiswa
hmahony6m	3Kbwo0V0ZhJ6	mahasiswa
cortells6n	W7VgmIic	mahasiswa
cmcduffy6o	PWFi5pdgC8	mahasiswa
hsedgwick6p	C8KHJ5am	mahasiswa
mpetters6q	HCtEMvh	mahasiswa
sfantone6r	nqWssNTW1h	mahasiswa
yivers6t	NeqLFI	mahasiswa
ekyndred6u	I8oLBoZvAqnO	mahasiswa
dambrose6w	2VpPfsYwi	mahasiswa
itowlson6x	A3zQg9uUf4Z	mahasiswa
dvonnassau6z	igiOZ2sX	mahasiswa
johern71	r98BHD	mahasiswa
vstolting72	d0w0PJ	mahasiswa
gchappel73	Zi0oRouK2	mahasiswa
jmaruszewski74	B0dzDp	mahasiswa
lthumann75	Jqa7b9i	mahasiswa
jfairnie76	FpRD79R	mahasiswa
juman78	FZqGwk0f	mahasiswa
rmatuszyk79	IKFkGoY	mahasiswa
mmalham7a	PH9FhJCAExV3	mahasiswa
dgiacobilio7b	IiWLjz	mahasiswa
gscreaton7c	Tiictwr	mahasiswa
ntarborn7e	4bEgnTEY7gY	mahasiswa
gyarham7f	tEIBZ40	mahasiswa
gtytler7g	PwEBzcVl	mahasiswa
lellerbeck7h	GtlauSKqf	mahasiswa
ddodds7i	FtcFTkGuJ	mahasiswa
rphlipon7j	ZQz3ey6	mahasiswa
rkeiling7k	fkXSTmi	mahasiswa
ezeal7l	cxo8xkIEU3	mahasiswa
lbiggerdike7m	T2GWCC5bq	mahasiswa
whaslam7n	IdZRldUFvZV	mahasiswa
smccurlye7o	JgrnNs	mahasiswa
ctwining7q	BMYIIvW	mahasiswa
mpuffett7u	WPxL6kBnV6O	mahasiswa
kredhouse7v	bR3OH64hey	mahasiswa
pbalderson7x	oIHQaYmNebMw	mahasiswa
dlorait7y	wFFOKqI	mahasiswa
rglazyer80	NVE4Sd8Nq	mahasiswa
lacres81	TlagW8pnO	mahasiswa
jvanderbaaren82	w2hXUls	mahasiswa
oalejandro83	KEpRpEOuLY	mahasiswa
ctame84	nqJCb5P	mahasiswa
gcoghill85	3voFKfGTx	mahasiswa
nmcmonies86	WcAodGVt	mahasiswa
vcarus87	hVT8SJs	mahasiswa
fscotfurth88	6YJRPsr	mahasiswa
rfullicks8a	woBE3o2GHhS	mahasiswa
fkeward8b	ECEY6P	mahasiswa
\.


--
-- Data for Name: pengumuman; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY pengumuman (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username, judul, isi) FROM stdin;
2018-04-16	1	99091	bfransinelli2n	semper sapien	Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.
2017-11-15	3	16235	cdurrad61	consectetuer	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.
2017-05-25	4	37889	agallie5r	ac	Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.
2018-02-02	6	72067	blavers2v	purus eu	Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.
2018-03-22	7	56652	bcakes7s	amet sem	Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.
2018-02-11	8	26459	amaydway5f	posuere felis	In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.
2017-09-05	9	81397	cmccollum7z	hendrerit	Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.
2017-08-24	10	35819	cbromet3t	pede	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.
2017-05-04	11	62235	blindbergv	id luctus	Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.
2018-01-21	13	39750	ajoanic5e	odio cras	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.
2017-06-19	14	15842	cflaubert4m	gravida sem	Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.
2017-10-08	15	4942	chenker43	mi nulla	Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.
2018-04-19	16	79051	arizzi50	faucibus cursus	Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.
2018-01-10	17	21793	ajoanic5e	lacus	Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.
2017-08-06	18	33359	asodeau7w	ligula	Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.
2017-05-25	2	84840	aferrao7t	nisl	Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci
2018-03-14	5	49570	ajoanic5e	tincidunt eu	Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in
2018-03-13	12	10089	cludovici7p	nunc	Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrice
2017-10-01	19	91678	aheller6s	id luctus	Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.
2017-10-09	20	91748	agallie5r	sed accumsan	Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.
2018-03-05	21	94848	bberrick3u	metus	Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.
2018-02-09	22	21608	cjepson49	quis	Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.
2017-10-15	23	25797	cbromet3t	ipsum primis	Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.
2017-10-13	24	53893	agallie5r	ante vel	Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.
2018-04-07	25	59564	bcooley16	in ante	Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.
2018-04-15	26	90191	awalcar6v	aliquet massa	Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.
2017-11-01	27	23763	cgarwood1q	nibh	Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.
2018-03-19	28	80238	blindbergv	habitasse platea	Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.
2017-05-25	29	38326	cgarwood1q	posuere	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.
2017-08-27	30	42535	cgilleon38	viverra	Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.
2017-06-03	31	4591	aomrod53	in porttitor	Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.
2017-09-06	33	65276	bbarracks70	leo odio	Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.
2017-12-14	32	48686	bfergusson89	sollicitudin	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.
2017-06-30	34	9291	aratray5x	est	Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.
2017-08-26	35	578	bberrick3u	feugiat	Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.
2017-06-25	36	10009	cjepson49	nec	Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.
2018-02-20	37	53938	aferrao7t	hac habitasse	Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
2018-03-26	38	47339	araoult28	lacinia	Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.
2017-10-20	40	48445	bhallward3i	in imperdiet	Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.
2017-09-30	41	79085	chenker43	in eleifend	Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.
2017-05-17	42	19546	awalcar6v	integer	Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.
2017-09-02	44	29760	atocher5w	pretium	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.
2017-11-06	45	20890	agallie5r	mauris	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.
2018-04-14	46	43942	cmccollum7z	curabitur in	Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.
2018-01-06	47	92838	cludovici7p	congue	Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.
2017-07-04	48	8041	alambrookn	sociis	Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
2017-11-09	43	37058	bcooley16	at nibh	Donec diam neque, vestibsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet 
2017-09-04	39	27651	cflaubert4m	sagittis	Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus 
2017-12-16	49	61320	bcardiff45	consequat	Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.
2018-01-21	50	33199	awalcar6v	ultrices libero	In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.
\.


--
-- Data for Name: riwayat_akademik; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY riwayat_akademik (no_urut, npm, semester, tahun_ajaran, jumlah_sks, ips, lampiran) FROM stdin;
1	1306254708	1	2000/2001	12	3.24000000000000021	pretium_iaculis.xls
2	1706358415	2	2000/2001	22	3.25999999999999979	in.xls
3	1206526391	1	2001/2002	18	2.81999999999999984	interdum_mauris.doc
4	1006447470	2	2001/2002	24	3.08000000000000007	integer.ppt
5	1206296516	1	2002/2003	23	2.93999999999999995	platea_dictumst.xls
6	1506238587	2	2002/2003	13	1.82000000000000006	turpis.xls
7	1806025248	1	2003/2004	21	3.18999999999999995	at_ipsum.ppt
8	1806273566	2	2003/2004	14	2.37000000000000011	mauris.xls
9	1106566949	1	2004/2005	15	2.41999999999999993	tempus.ppt
10	1606166321	2	2004/2005	13	2.62000000000000011	bibendum_felis.ppt
11	1806390205	1	2005/2006	21	3.93000000000000016	volutpat_erat.xls
12	1806239573	2	2005/2006	14	2.37999999999999989	elementum_eu_interdum.xls
13	1306238740	1	2006/2007	24	1.69999999999999996	a_libero_nam.xls
14	1406227772	2	2006/2007	12	2.20000000000000018	in.ppt
15	1506479098	1	2007/2008	13	3.83999999999999986	turpis.ppt
1	1206138691	2	2000/2001	18	2.79000000000000004	augue_vestibulum_ante.ppt
2	1906913560	1	2000/2001	24	1.56000000000000005	sem_sed_sagittis.doc
3	1406760488	2	2001/2002	22	1.54000000000000004	non_mattis.ppt
4	1806626490	1	2001/2002	21	2.58999999999999986	in_hac.xls
5	1306307144	2	2002/2003	15	2.18000000000000016	erat.xls
6	1306497428	1	2002/2003	21	3.14999999999999991	lobortis.ppt
7	1806185670	2	2003/2004	21	2.43000000000000016	nullam_porttitor.xls
8	1206910208	1	2003/2004	15	2.83000000000000007	potenti.doc
9	1506453445	2	2004/2005	17	1.79000000000000004	mi.xls
10	1306254708	1	2004/2005	12	3.06000000000000005	ac_diam_cras.ppt
11	1706358415	2	2005/2006	24	2.49000000000000021	orci.ppt
12	1206526391	1	2005/2006	19	2.99000000000000021	sapien_sapien_non.ppt
13	1006447470	2	2006/2007	19	3.62999999999999989	justo_in.xls
14	1206296516	1	2006/2007	14	3.83999999999999986	luctus_et.doc
15	1506238587	2	2007/2008	23	3.16999999999999993	vel_augue_vestibulum.ppt
1	1806025248	1	2000/2001	16	3.79000000000000004	amet_diam_in.ppt
2	1806273566	2	2000/2001	15	3.16999999999999993	at.xls
3	1106566949	1	2001/2002	17	2.25999999999999979	justo_lacinia_eget.xls
4	1606166321	2	2001/2002	12	2.93999999999999995	curabitur_convallis.xls
5	1806390205	1	2002/2003	13	3.41999999999999993	amet.ppt
6	1806239573	2	2002/2003	12	2.77000000000000002	in.ppt
7	1306238740	1	2003/2004	14	3.33999999999999986	condimentum_curabitur_in.xls
8	1406227772	2	2003/2004	16	3.62000000000000011	quis_lectus_suspendisse.ppt
9	1506479098	1	2004/2005	15	1.6100000000000001	euismod_scelerisque_quam.xls
10	1206138691	2	2004/2005	15	3.39000000000000012	quis.xls
11	1906913560	1	2005/2006	13	2.95000000000000018	donec_dapibus_duis.ppt
12	1406760488	2	2005/2006	15	1.62999999999999989	sem.xls
13	1806626490	1	2006/2007	24	3.16000000000000014	tortor.ppt
14	1306307144	2	2006/2007	20	3.62999999999999989	sapien_varius.pdf
15	1306497428	1	2007/2008	15	1.78000000000000003	nisl_duis.ppt
1	1806185670	2	2000/2001	18	3.16999999999999993	posuere_cubilia.xls
2	1206910208	1	2000/2001	19	1.73999999999999999	augue_aliquam_erat.xls
3	1506453445	2	2001/2002	24	3.27000000000000002	odio_porttitor.ppt
4	1306254708	1	2001/2002	12	3.04000000000000004	primis_in_faucibus.doc
5	1706358415	2	2002/2003	21	1.55000000000000004	integer_non.ppt
6	1206526391	1	2002/2003	23	1.58000000000000007	vitae.ppt
7	1006447470	2	2003/2004	16	2.43000000000000016	habitasse_platea.xls
8	1206296516	1	2003/2004	23	2.91999999999999993	enim_sit_amet.pdf
12	1306497428	1	2005/2006	19	2.5299999999999998	consequat_varius.xls
13	1806185670	2	2006/2007	23	2.50999999999999979	sed.ppt
14	1206910208	1	2006/2007	19	3.83000000000000007	eget.xls
15	1506453445	2	2007/2008	22	2.47999999999999998	pede_libero.xls
9	1506238587	2	2004/2005	21	2.79000000000000004	in.ppt
10	1806025248	1	2004/2005	23	1.97999999999999998	volutpat.ppt
11	1806273566	2	2005/2006	14	3.31000000000000005	rutrum_neque.xls
12	1106566949	1	2005/2006	21	2.39999999999999991	eu_massa_donec.doc
13	1606166321	2	2006/2007	18	3.37999999999999989	velit_donec_diam.ppt
14	1806390205	1	2006/2007	20	3.95999999999999996	sapien_non_mi.ppt
15	1806239573	2	2007/2008	22	3.00999999999999979	odio_in.ppt
1	1306238740	1	2000/2001	18	2.4700000000000002	iaculis.xls
2	1406227772	2	2000/2001	15	1.58000000000000007	turpis_adipiscing.doc
3	1506479098	1	2001/2002	23	2.41999999999999993	ante_ipsum.pdf
4	1206138691	2	2001/2002	19	2.75999999999999979	tortor_eu_pede.xls
5	1906913560	1	2002/2003	15	3.60999999999999988	vestibulum_velit.doc
6	1406760488	2	2002/2003	20	2.02000000000000002	ipsum.xls
7	1806626490	1	2003/2004	24	3.83000000000000007	congue_vivamus_metus.ppt
8	1306307144	2	2003/2004	21	2.41999999999999993	risus.ppt
9	1306497428	1	2004/2005	22	1.59000000000000008	convallis_duis_consequat.xls
10	1806185670	2	2004/2005	16	1.84000000000000008	ut.ppt
11	1206910208	1	2005/2006	24	2.41999999999999993	sapien.ppt
12	1506453445	2	2005/2006	19	3.14000000000000012	curae_duis.xls
13	1306254708	1	2006/2007	15	1.5	porttitor_id.ppt
14	1706358415	2	2006/2007	14	3.29999999999999982	aenean_fermentum.ppt
15	1206526391	1	2007/2008	14	3.83999999999999986	pretium.xls
1	1006447470	2	2000/2001	13	2.27000000000000002	posuere_nonummy.xls
2	1206296516	1	2000/2001	15	2.08000000000000007	faucibus_accumsan_odio.xls
3	1506238587	2	2001/2002	20	2.95999999999999996	nisi.pdf
4	1806025248	1	2001/2002	16	3.58999999999999986	neque_sapien.xls
1	1306413101	2	2010/2011	14	3.87999999999999989	faucibus orci luctus et ultrices posuere
2	1406605255	2	2012/2013	24	3.20000000000000018	interdum eu tincidunt in leo maecenas
3	1106115142	2	2014/2015	20	2.54000000000000004	nascetur ridiculus mus etiam vel augue
4	1006096383	2	2018/2019	16	2.06999999999999984	sit amet eleifend pede libero quis
5	1406667224	2	2012/2013	17	3.75	risus semper porta volutpat quam pede
6	1106941116	1	2012/2013	22	2.10000000000000009	erat nulla tempus vivamus in felis
7	1706002472	2	2014/2015	16	2.06000000000000005	nunc commodo placerat praesent blandit nam
8	1106662519	2	2016/2017	13	3.81000000000000005	maecenas pulvinar lobortis est phasellus sit
9	1606517541	1	2012/2013	24	2.66999999999999993	in est risus auctor sed tristique
10	1706193218	2	2010/2011	17	2.39999999999999991	elit proin risus praesent lectus vestibulum
11	1206900812	2	2015/2016	19	3.35999999999999988	pretium iaculis diam erat fermentum justo
12	1906772612	1	2013/2014	19	2.60999999999999988	mi in porttitor pede justo eu
13	1206069390	1	2013/2014	18	2.7799999999999998	montes nascetur ridiculus mus etiam vel
14	1606846738	2	2010/2011	24	2.5299999999999998	suscipit ligula in lacus curabitur at
15	1006792295	1	2015/2016	19	3.83999999999999986	quam nec dui luctus rutrum nulla
16	1106022312	2	2017/2018	13	3.81000000000000005	faucibus orci luctus et ultrices posuere
17	1706431770	1	2010/2011	16	2.83999999999999986	ipsum ac tellus semper interdum mauris
18	1506139378	2	2013/2014	13	2.43000000000000016	ut massa quis augue luctus tincidunt
19	1206191237	1	2015/2016	19	3.20999999999999996	vel ipsum praesent blandit lacinia erat
20	1506452868	1	2012/2013	23	3.95000000000000018	tempus sit amet sem fusce consequat
21	1406633378	2	2012/2013	17	2.47999999999999998	porta volutpat quam pede lobortis ligula
22	1906149671	2	2010/2011	13	3.20000000000000018	tempus sit amet sem fusce consequat
23	1006124360	1	2016/2017	16	2.18999999999999995	ut dolor morbi vel lectus in
24	1306427173	1	2015/2016	21	2.12000000000000011	posuere felis sed lacus morbi sem
25	1306477028	1	2016/2017	17	3.52000000000000002	elementum in hac habitasse platea dictumst
26	1206910208	1	2014/2015	17	2.41999999999999993	mollis molestie lorem quisque ut erat
27	1406029630	1	2013/2014	21	3.39000000000000012	pede ullamcorper augue a suscipit nulla
28	1106022312	2	2016/2017	23	3.33000000000000007	praesent blandit lacinia erat vestibulum sed
29	1306133042	2	2010/2011	18	2.62999999999999989	felis fusce posuere felis sed lacus
30	1406481802	1	2017/2018	13	2.04999999999999982	dictumst etiam faucibus cursus urna ut
31	1406878965	1	2010/2011	15	3.31000000000000005	ut at dolor quis odio consequat
32	1206762117	1	2011/2012	17	2.35000000000000009	curabitur gravida nisi at nibh in
33	1306151865	2	2017/2018	13	2.93999999999999995	tincidunt lacus at velit vivamus vel
59	1906347959	2	2016/2017	14	2.08000000000000007	morbi ut odio cras mi pede
60	1806360513	1	2018/2019	19	2.5	ridiculus mus vivamus vestibulum sagittis sapien
61	1606033596	1	2011/2012	15	2.89999999999999991	massa donec dapibus duis at velit
62	1506372913	2	2018/2019	22	3.68000000000000016	vel lectus in quam fringilla rhoncus
63	1506994587	1	2014/2015	24	3.62999999999999989	lorem quisque ut erat curabitur gravida
64	1506372913	2	2013/2014	19	2.87999999999999989	id ligula suspendisse ornare consequat lectus
65	1506238587	1	2013/2014	13	2.85999999999999988	interdum venenatis turpis enim blandit mi
66	1306169650	1	2015/2016	20	3.45999999999999996	ut massa quis augue luctus tincidunt
67	1506453445	2	2015/2016	20	3.24000000000000021	in consequat ut nulla sed accumsan
68	1106566949	2	2017/2018	13	3.04999999999999982	eu felis fusce posuere felis sed
69	1106590538	1	2011/2012	21	2.68999999999999995	mi integer ac neque duis bibendum
70	1206910208	1	2017/2018	21	2.66999999999999993	eget nunc donec quis orci eget
71	1206781746	1	2015/2016	14	2.83999999999999986	et ultrices posuere cubilia curae donec
72	1406760488	1	2018/2019	16	2.93000000000000016	sem sed sagittis nam congue risus
73	1806624411	1	2011/2012	18	3.58999999999999986	ultrices vel augue vestibulum ante ipsum
74	1506315308	1	2014/2015	15	3.14999999999999991	porta volutpat quam pede lobortis ligula
75	1206138691	2	2013/2014	23	2.93999999999999995	sit amet justo morbi ut odio
76	1606117895	1	2012/2013	21	2.41000000000000014	vel accumsan tellus nisi eu orci
77	1906347959	1	2015/2016	24	3.16999999999999993	neque aenean auctor gravida sem praesent
78	1806827299	1	2018/2019	20	2.54999999999999982	pede lobortis ligula sit amet eleifend
79	1606117895	2	2016/2017	19	2.18999999999999995	laoreet ut rhoncus aliquet pulvinar sed
80	1306446209	1	2013/2014	20	3.4700000000000002	primis in faucibus orci luctus et
81	1406203323	2	2010/2011	14	3.72999999999999998	accumsan felis ut at dolor quis
82	1506452868	2	2018/2019	13	2.29000000000000004	at n\n\n;\n
83	1306413101	1	2016/2017	22	2.25999999999999979	nulla tellus in sagittis dui vel
84	1406633378	1	2014/2015	22	2.24000000000000021	nisl aenean lectus pellentesque eget nunc
85	1206910208	1	2012/2013	21	2.60000000000000009	tempor convallis nulla neque libero convallis
86	1706431770	1	2014/2015	22	3.85999999999999988	porta volutpat quam pede lobortis ligula
87	1806974954	2	2017/2018	18	2.12999999999999989	dui luctus rutrum nulla tellus in
88	1906633707	2	2011/2012	20	3.68999999999999995	parturient montes nascetur ridiculus mus vivamus
89	1606687187	1	2013/2014	19	3.4700000000000002	eu interdum eu tincidunt in leo
90	1306512469	1	2014/2015	22	2.79999999999999982	massa tempor convallis nulla neque libero
91	1406205713	2	2010/2011	17	2.33999999999999986	magnis dis parturient montes nascetur ridiculus
92	1106757668	2	2015/2016	20	3.66999999999999993	turpis integer aliquet massa id lobortis
93	1006124360	1	2015/2016	23	3.52000000000000002	libero rutrum ac lobortis vel dapibus
94	1206701835	1	2015/2016	17	3.20999999999999996	sem fusce consequat nulla nisl nunc
95	1606935727	2	2016/2017	23	3.68999999999999995	at ipsum ac tellus semper interdum
96	1006976354	1	2013/2014	24	2.37000000000000011	integer ac neque duis bibendum morbi
97	1406390739	1	2011/2012	19	3.08999999999999986	mollis molestie lorem quisque ut erat
98	1106824845	1	2017/2018	15	2.06000000000000005	sed lacus morbi sem mauris laoreet
99	1906429508	2	2011/2012	20	2.52000000000000002	quam sapien varius ut blandit non
100	1506247620	1	2012/2013	21	2.91999999999999993	platea dictumst aliquam augue quam sollicitudin
101	1106824845	2	2014/2015	17	3.06999999999999984	placerat ante nulla justo aliquam quis
102	1106989013	2	2018/2019	15	2.08000000000000007	cursus id turpis integer aliquet massa
103	1606189532	2	2017/2018	14	2.35999999999999988	commodo vulputate justo in blandit ultrices
104	1206265578	1	2018/2019	19	2.68000000000000016	suspendisse potenti nullam porttitor lacus at
116	1306151865	1	2015/2016	21	2.20000000000000018	praesent id massa id nisl venenatis
117	1406425546	2	2012/2013	20	2.41000000000000014	mi nulla ac enim in tempor
118	1406566237	1	2015/2016	21	3.20000000000000018	laoreet ut rhoncus aliquet pulvinar sed
119	1306476813	1	2018/2019	19	3.20999999999999996	lobortis vel dapibus at diam nam
120	1306323828	2	2016/2017	14	2.68000000000000016	luctus tincidunt nulla mollis molestie lorem
121	1306446209	1	2017/2018	20	2.91000000000000014	amet sapien dignissim vestibulum vestibulum ante
122	1806273566	2	2018/2019	14	3.08000000000000007	nulla suscipit ligula in lacus curabitur
123	1606189532	1	2012/2013	14	2.58999999999999986	pretium iaculis justo in hac habitasse
124	1106022312	1	2013/2014	13	3.58000000000000007	in hac habitasse platea dictumst etiam
125	1106022312	2	2013/2014	18	3.7200000000000002	mi in porttitor pede justo eu
126	1506366816	2	2013/2014	23	3.66999999999999993	eu magna vulputate luctus cum sociis
127	1206953084	2	2011/2012	22	2.35999999999999988	pellentesque eget nunc donec quis orci
128	1006976354	2	2010/2011	24	3.22999999999999998	dolor sit amet consectetuer adipiscing elit
129	1006303784	2	2016/2017	15	3.41000000000000014	eget elit sodales scelerisque mauris sit
130	1006671469	2	2012/2013	23	3.77000000000000002	nascetur ridiculus mus vivamus vestibulum sagittis
131	1306179863	1	2018/2019	16	3.12000000000000011	eu felis fusce posuere felis sed
132	1606117895	1	2017/2018	18	2.31000000000000005	in blandit ultrices enim lorem ipsum
133	1606033596	1	2012/2013	17	2.83999999999999986	non mattis pulvinar nulla pede ullamcorper
134	1406760488	2	2011/2012	24	2.04999999999999982	eu orci mauris lacinia sapien quis
135	1506139378	2	2013/2014	17	3.68999999999999995	a ipsum integer a nibh in
136	1706603949	1	2017/2018	15	2.49000000000000021	aliquam erat volutpat in congue etiam
137	1606517541	2	2010/2011	21	3.56000000000000005	fermentum justo nec condimentum neque sapien
138	1406197577	1	2016/2017	17	2.18999999999999995	molestie sed justo pellentesque viverra pede
139	1006124179	2	2017/2018	16	3.85999999999999988	justo in hac habitasse platea dictumst
140	1606562674	1	2018/2019	23	3.22999999999999998	erat nulla tempus vivamus in felis
141	1706002472	1	2013/2014	17	3.64999999999999991	vel sem sed sagittis nam congue
142	1906429508	2	2017/2018	21	3.62999999999999989	tellus nisi eu orci mauris lacinia
143	1206914821	2	2014/2015	22	2.18000000000000016	sollicitudin ut suscipit a feugiat et
144	1306218532	2	2013/2014	17	2.81999999999999984	imperdiet nullam orci pede venenatis non
145	1906913560	1	2013/2014	13	2.95000000000000018	mi in porttitor pede justo eu
146	1006124360	2	2018/2019	13	3.74000000000000021	condimentum neque sapien placerat ante nulla
147	1706145505	1	2015/2016	16	2.08999999999999986	fringilla rhoncus mauris enim leo rhoncus
148	1206365466	2	2010/2011	13	2.85000000000000009	vivamus in felis eu sapien cursus
149	1806625928	1	2018/2019	17	2.60000000000000009	aenean fermentum donec ut mauris eget
150	1906429508	2	2010/2011	24	2.56000000000000005	orci luctus et ultrices posuere cubilia
151	1306877104	2	2018/2019	17	3.68999999999999995	platea dictumst aliquam augue quam sollicitudin
152	1706431770	2	2010/2011	16	2.66000000000000014	ipsum dolor sit amet consectetuer adipiscing
153	1706721082	1	2014/2015	23	2.45999999999999996	enim sit amet nunc viverra dapibus
154	1606117895	1	2017/2018	20	2.7200000000000002	vel est donec odio justo sollicitudin
155	1706721082	1	2014/2015	15	2.95999999999999996	nulla ac enim in tempor turpis
156	1006340824	1	2011/2012	19	3.33000000000000007	sit amet diam in magna bibendum
157	1406385872	1	2018/2019	19	3.7200000000000002	quisque arcu libero rutrum ac lobortis
158	1906429508	1	2014/2015	15	3.87999999999999989	in hac habitasse platea dictumst morbi
159	1706137145	2	2015/2016	24	2.62000000000000011	maecenas tristique est et tempus semper
160	1906320852	2	2015/2016	20	2.93000000000000016	blandit mi in porttitor pede justo
161	1206265578	2	2017/2018	16	3.64999999999999991	in est risus auctor sed tristique
162	1806625928	2	2017/2018	15	2.16000000000000014	porttitor pede justo eu massa donec
163	1006950485	1	2015/2016	15	3.18999999999999995	interdum mauris ullamcorper purus sit amet
164	1806239573	2	2018/2019	17	2.81000000000000005	ipsum primis in faucibus orci luctus
165	1706854005	1	2010/2011	23	3.77000000000000002	ipsum primis in faucibus orci luctus
166	1806455055	1	2011/2012	20	2.39999999999999991	penatibus et magnis dis parturient montes
167	1006124360	1	2011/2012	13	2.85000000000000009	metus vitae ipsum aliquam non mauris
168	1306413101	1	2015/2016	22	3.0299999999999998	eu mi nulla ac enim in
169	1706358415	2	2010/2011	22	2.56999999999999984	sed lacus morbi sem mauris laoreet
170	1706698635	2	2012/2013	18	2.87000000000000011	proin leo odio porttitor id consequat
171	1606846738	1	2016/2017	15	2.54999999999999982	gravida nisi at nibh in hac
172	1906149671	2	2013/2014	18	3.66000000000000014	non pretium quis lectus suspendisse potenti
173	1506479098	2	2012/2013	13	3.31000000000000005	in ante vestibulum ante ipsum primis
174	1106989013	2	2016/2017	15	2.64999999999999991	enim lorem ipsum dolor sit amet
175	1806318404	1	2011/2012	13	2.33999999999999986	odio curabitur convallis duis consequat dui
176	1206701835	1	2018/2019	16	3.10000000000000009	non quam nec dui luctus rutrum
177	1306497428	2	2018/2019	15	2.04999999999999982	nulla elit ac nulla sed vel
178	1406797996	2	2016/2017	22	2.66000000000000014	ultrices mattis odio donec vitae nisi
179	1906149671	2	2010/2011	17	2.06999999999999984	consequat lectus in est risus auctor
5	1806273566	2	2002/2003	18	3.49000000000000021	quis.xls
6	1106566949	1	2002/2003	23	2.20000000000000018	metus.doc
7	1606166321	2	2003/2004	17	3.85999999999999988	malesuada.ppt
8	1806390205	1	2003/2004	13	2.20999999999999996	aenean.ppt
9	1806239573	2	2004/2005	21	3.85000000000000009	aliquet.pdf
10	1306238740	1	2004/2005	24	2.14000000000000012	at_turpis_a.pdf
11	1406227772	2	2005/2006	24	3.20999999999999996	sed_accumsan.xls
12	1506479098	1	2005/2006	22	3.22999999999999998	tortor_risus.xls
13	1206138691	2	2006/2007	12	1.81000000000000005	convallis_duis.ppt
14	1906913560	1	2006/2007	21	2.04000000000000004	in.ppt
15	1406760488	2	2007/2008	18	2.35999999999999988	neque_aenean_auctor.pdf
1	1806626490	1	2000/2001	24	1.55000000000000004	ut_erat.xls
2	1306307144	2	2000/2001	15	3.56000000000000005	quis.ppt
3	1306497428	1	2001/2002	23	1.84000000000000008	posuere_cubilia.pdf
4	1806185670	2	2001/2002	21	3.18999999999999995	eget_massa_tempor.xls
5	1206910208	1	2002/2003	13	2.08000000000000007	pede.ppt
6	1506453445	2	2002/2003	16	2.85000000000000009	scelerisque.ppt
7	1306254708	1	2003/2004	20	2.95000000000000018	pellentesque_ultrices.ppt
8	1706358415	2	2003/2004	19	2.37999999999999989	augue_quam_sollicitudin.pdf
9	1206526391	1	2004/2005	14	2.14999999999999991	enim.xls
10	1006447470	2	2004/2005	17	3.95999999999999996	blandit.ppt
11	1206296516	1	2005/2006	16	3.12999999999999989	sed_augue_aliquam.xls
12	1506238587	2	2005/2006	24	2.18999999999999995	leo.ppt
13	1806025248	1	2006/2007	16	3.85000000000000009	praesent_blandit_nam.xls
14	1806273566	2	2006/2007	15	3.35000000000000009	sapien.ppt
15	1106566949	1	2007/2008	15	2.60999999999999988	maecenas.pdf
1	1606166321	2	2000/2001	16	3.64000000000000012	sit.xls
2	1806390205	1	2000/2001	19	2.12999999999999989	ipsum_praesent.xls
3	1806239573	2	2001/2002	23	3.08999999999999986	ac.xls
4	1306238740	1	2001/2002	22	3.74000000000000021	justo_in_hac.ppt
5	1406227772	2	2002/2003	20	3.64000000000000012	phasellus_id_sapien.xls
6	1506479098	1	2002/2003	24	3.89999999999999991	condimentum.doc
7	1206138691	2	2003/2004	12	3.4700000000000002	magnis_dis.ppt
8	1906913560	1	2003/2004	20	2.79999999999999982	nam_ultrices.pdf
9	1406760488	2	2004/2005	22	1.58000000000000007	nisl_aenean_lectus.xls
10	1806626490	1	2004/2005	13	3.95000000000000018	orci_nullam.xls
11	1306307144	2	2005/2006	18	3.75999999999999979	at.ppt
180	1106022312	1	2014/2015	22	2.04999999999999982	eros viverra eget congue eget semper
181	1506030310	2	2018/2019	14	2.2799999999999998	in felis eu sapien cursus vestibulum
182	1906883395	1	2018/2019	20	3.79999999999999982	consectetuer eget rutrum at lorem integer
183	1406452300	2	2017/2018	14	2.10000000000000009	viverra diam vitae quam suspendisse potenti
184	1306240346	2	2010/2011	20	2	nulla eget eros elementum pellentesque quisque
185	1806827299	1	2013/2014	16	2.00999999999999979	sed augue aliquam erat volutpat in
186	1006976354	1	2011/2012	17	3.58000000000000007	cum sociis natoque penatibus et magnis
187	1406452300	2	2015/2016	20	3.08000000000000007	odio elementum eu interdum eu tincidunt
188	1306151865	2	2011/2012	17	3.68000000000000016	velit nec nisi vulputate nonummy maecenas
189	1906913560	1	2016/2017	19	2.64999999999999991	vehicula condimentum curabitur in libero ut
190	1706854005	1	2015/2016	15	2.14999999999999991	amet eros suspendisse accumsan tortor quis
191	1806625928	1	2011/2012	24	3.56999999999999984	maecenas ut massa quis augue luctus
192	1606783983	2	2012/2013	16	3.89999999999999991	sollicitudin mi sit amet lobortis sapien
193	1306497428	1	2017/2018	16	2.83000000000000007	mi nulla ac enim in tempor
194	1806390205	1	2013/2014	21	3.43999999999999995	elementum pellentesque quisque porta volutpat erat
195	1906913560	1	2015/2016	24	2.93999999999999995	est quam pharetra magna ac consequat
196	1506453445	1	2011/2012	14	2.79000000000000004	id lobortis convallis tortor risus dapibus
197	1106941116	1	2012/2013	17	3.83000000000000007	suspendisse accumsan tortor quis turpis sed
198	1006792295	1	2016/2017	13	2.93000000000000016	quam nec dui luctus rutrum nulla
199	1606362154	2	2017/2018	22	2.75999999999999979	donec odio justo sollicitudin ut suscipit
200	1606847536	1	2011/2012	19	2.5299999999999998	sed justo pellentesque viverra pede ac
201	1306476813	1	2014/2015	17	3.70999999999999996	libero convallis eget eleifend luctus ultricies
202	1206467185	1	2012/2013	23	2.7200000000000002	consectetuer adipiscing elit proin risus praesent
203	1006124179	2	2016/2017	24	3.97999999999999998	tincidunt in leo maecenas pulvinar lobortis
204	1806390205	1	2015/2016	18	2.37000000000000011	vulputate nonummy maecenas tincidunt lacus at
205	1906084911	1	2016/2017	19	2.7799999999999998	nonummy maecenas tincidunt lacus at velit
206	1106603614	1	2010/2011	13	2.22999999999999998	vestibulum eget vulputate ut ultrices vel
207	1806974954	1	2014/2015	16	3.79999999999999982	vel nulla eget eros elementum pellentesque
208	1206524282	1	2010/2011	18	3.85999999999999988	habitasse platea dictumst maecenas ut massa
209	1706137145	1	2012/2013	20	3.66000000000000014	quis orci nullam molestie nibh in
210	1106757668	2	2015/2016	19	2.68000000000000016	sit amet justo morbi ut odio
211	1406557975	1	2015/2016	14	2.58999999999999986	justo etiam pretium iaculis justo in
212	1906913560	2	2018/2019	21	3.93999999999999995	orci nullam molestie nibh in lectus
213	1906588075	2	2018/2019	22	3.39999999999999991	turpis nec euismod scelerisque quam turpis
214	1306240346	2	2018/2019	19	3.35999999999999988	eu sapien cursus vestibulum proin eu
215	1706374528	1	2011/2012	22	2.7200000000000002	posuere felis sed lacus morbi sem
216	1606340207	1	2015/2016	19	4	augue a suscipit nulla elit ac
217	1806538813	1	2012/2013	24	2.14999999999999991	morbi non lectus aliquam sit amet
218	1006096383	1	2010/2011	22	3.43999999999999995	vestibulum velit id pretium iaculis diam
219	1206467185	1	2013/2014	21	2.56999999999999984	sapien varius ut blandit non interdum
220	1906588075	2	2012/2013	18	3.18999999999999995	nisl duis ac nibh fusce lacus
221	1006889349	1	2011/2012	22	2.18999999999999995	ut erat curabitur gravida nisi at
222	1506551420	2	2017/2018	19	3.37000000000000011	nulla ultrices aliquet maecenas leo odio
223	1106603614	2	2017/2018	19	3.60999999999999988	phasellus in felis donec semper sapien
\.


--
-- Data for Name: skema_beasiswa; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY skema_beasiswa (kode, nama, jenis, deskripsi, nomor_identitas_donatur) FROM stdin;
99091	AETNA Foundation/NMF Healthcare Leadership Program	 Bantuan Belajar 	Sed ante. Vivamus tortor. Duis mattis egestas metu	1
84840	AICPA Accountemps Student Scholarship	Prestasi Akademik 	Duis bibendum, felis sed interdum venenatis, turpi	2
16235	ACFEF Johnson & Wales Scholarship	 Tugas Akhir	Proin leo odio, porttitor id, consequat in, conseq	3
37889	AICPA Accountemps Student Scholarship	 Tugas Akhir	Phasellus in felis. Donec semper sapien a libero. 	4
49570	ACF Youth in Foster Care Scholarship Program	 Tugas Akhir	Nullam sit amet turpis elementum ligula vehicula c	5
72067	ACF Woodcock Family Education Scholarship Program	 Tugas Akhir	Nullam porttitor lacus at turpis. Donec posuere me	6
56652	ABA Member Scholarships	 Bantuan Belajar 	Maecenas tristique, est et tempus semper, est quam	7
26459	ANS Undergraduate Scholarship	 Tugas Akhir	In sagittis dui vel nisl. Duis ac nibh. Fusce lacu	8
81397	ACF Barnes W. Rose	 Tugas Akhir	Proin eu mi. Nulla ac enim. In tempor, turpis nec 	9
35819	AMTIE Scholarships	 Tugas Akhir	Vestibulum ac est lacinia nisi venenatis tristique	10
62235	AMTIE Scholarships	 Bantuan Belajar 	In congue. Etiam justo. Etiam pretium iaculis just	11
10089	ACF James Ledwith Memorial Scholarship	 Tugas Akhir	Etiam vel augue. Vestibulum rutrum rutrum neque. A	12
39750	AJLI Scholarship Program to Study in Israel	 Tugas Akhir	Quisque porta volutpat erat. Quisque erat eros, vi	13
15842	AFTH Birth Parent Scholarship Fund	Prestasi Akademik 	Etiam vel augue. Vestibulum rutrum rutrum neque. A	14
4942	AFSA High School Senior Merit and Community Servic	Prestasi Akademik 	Phasellus in felis. Donec semper sapien a libero. 	15
79051	ACHE Two-Year College Academic Scholarship Program	 Bantuan Belajar 	Nullam porttitor lacus at turpis. Donec posuere me	16
21793	AG Bell College Scholarship Awards	Prestasi Akademik 	Curabitur in libero ut massa volutpat convallis. M	17
33359	 Inc. Scholarship	Prestasi Akademik 	Cum sociis natoque penatibus et magnis dis parturi	18
91678	ACF Charlie and Kathy Barnhart and Ken Haynes	 Tugas Akhir	Cum sociis natoque penatibus et magnis dis parturi	19
91748	ACF Barnes W. Rose	Prestasi Akademik 	Cras non velit nec nisi vulputate nonummy. Maecena	20
94848	ACF David R. Woodling Memorial Scholarship	Prestasi Akademik 	Duis consequat dui nec nisi volutpat eleifend. Don	21
21608	ACF American Advertising Federation-NM Scholarship	 Tugas Akhir	Quisque porta volutpat erat. Quisque erat eros, vi	22
25797	ALA Christopher J. Hoy/ERT Scholarship	Prestasi Akademik 	Aenean lectus. Pellentesque eget nunc. Donec quis 	23
53893	AMS Graduate Fellowships	Prestasi Akademik 	Nam ultrices, libero non mattis pulvinar, nulla pe	24
59564	ACFEF Culinary Institute of America Scholarship	 Tugas Akhir	Lorem ipsum dolor sit amet, consectetuer adipiscin	25
90191	ACF Kiwanis Club of Albuquerque Scholarship Progra	Prestasi Akademik 	Nam ultrices, libero non mattis pulvinar, nulla pe	26
23763	ACF Sussman-Miller Educational Assistance Award Pr	 Tugas Akhir	In congue. Etiam justo. Etiam pretium iaculis just	27
80238	AFSA High School Essay Contest	Prestasi Akademik 	Pellentesque at nulla. Suspendisse potenti. Cras i	28
38326	ACHE Foster G. McGaw Graduate Student Scholarship	 Tugas Akhir	Sed sagittis. Nam congue, risus semper porta volut	29
42535	AETNA Foundation/NMF Healthcare Leadership Program	 Bantuan Belajar 	Etiam vel augue. Vestibulum rutrum rutrum neque. A	30
4591	ANS Incoming Freshman Scholarship	 Bantuan Belajar 	Sed sagittis. Nam congue, risus semper porta volut	31
48686	ABPA Harrington-Arthur Memorial Scholarship Essay 	 Tugas Akhir	Aliquam quis turpis eget elit sodales scelerisque.	32
65276	 Inc. Scholarship	 Tugas Akhir	In quis justo. Maecenas rhoncus aliquam lacus. Mor	33
9291	AEF Scholarship	 Bantuan Belajar 	Integer tincidunt ante vel ipsum. Praesent blandit	34
578	ACF James Ledwith Memorial Scholarship	 Tugas Akhir	Integer ac leo. Pellentesque ultrices mattis odio.	35
10009	AFTH Birth Parent Scholarship Fund	 Tugas Akhir	Proin interdum mauris non ligula pellentesque ultr	36
53938	 and Control Graduate Award	 Tugas Akhir	Etiam vel augue. Vestibulum rutrum rutrum neque. A	37
47339	AFA Teens for Alzheimer's Awareness College Schola	 Tugas Akhir	Aenean lectus. Pellentesque eget nunc. Donec quis 	38
27651	ABA Academic Merit Scholarship	 Tugas Akhir	Vestibulum quam sapien, varius ut, blandit non, in	39
48445	ALS Family Charitable Foundation	 Bantuan Belajar 	Nullam sit amet turpis elementum ligula vehicula c	40
79085	ACHE Foster G. McGaw Graduate Student Scholarship	 Tugas Akhir	Proin interdum mauris non ligula pellentesque ultr	41
19546	ACIL Academic Scholarships	 Bantuan Belajar 	Etiam vel augue. Vestibulum rutrum rutrum neque. A	42
37058	AEF Scholarship	Prestasi Akademik 	Donec diam neque, vestibulum eget, vulputate ut, u	43
29760	 Jr. and Eva Rose Nichol Scholarship Program	 Bantuan Belajar 	Curabitur gravida nisi at nibh. In hac habitasse p	44
20890	ALA Tom & Roberta Drewes Scholarship	 Tugas Akhir	Praesent id massa id nisl venenatis lacinia. Aenea	45
43942	AFAS General Henry H. Arnold Education Grant Progr	 Bantuan Belajar 	Vestibulum ac est lacinia nisi venenatis tristique	46
92838	AFA Teens for Alzheimer's Awareness College Schola	Prestasi Akademik 	Phasellus sit amet erat. Nulla tempus. Vivamus in 	47
8041	ACF James Ledwith Memorial Scholarship	Prestasi Akademik 	Nam ultrices, libero non mattis pulvinar, nulla pe	48
61320	AMTIE Scholarships	Prestasi Akademik 	Aenean lectus. Pellentesque eget nunc. Donec quis 	49
33199	ACFEF Future Postsecondary Student Scholarships	 Tugas Akhir	Morbi non lectus. Aliquam sit amet diam in magna b	50
\.


--
-- Data for Name: skema_beasiswa_aktif; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY skema_beasiswa_aktif (kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, total_pembayaran, jumlah_pendaftar) FROM stdin;
33199	100	2017-12-03	2017-05-30	2005	in	63	0
99091	1	2017-06-20	2018-04-24	1990	ac	83	0
84840	2	2017-08-21	2017-10-06	2010	ut	52	0
16235	3	2017-11-30	2017-05-02	2001	ante	28	0
37889	4	2018-01-16	2017-05-26	1995	pretium	59	0
49570	5	2017-10-16	2017-08-02	2011	tellus	59	0
72067	6	2018-01-05	2017-09-14	1998	in	99	0
56652	7	2018-03-13	2017-05-09	1996	nullam	86	0
26459	8	2018-04-10	2017-08-14	2002	vel	81	0
81397	9	2017-07-17	2018-03-07	2012	mattis	30	0
35819	10	2017-08-08	2017-07-06	1984	ac	5	0
62235	11	2018-01-21	2018-02-24	2009	congue	40	0
10089	12	2018-01-22	2017-07-14	1999	justo	48	0
39750	13	2017-12-25	2017-12-06	1992	eu	94	0
15842	14	2018-02-08	2018-01-04	1990	praesent	18	0
4942	15	2018-04-22	2018-03-14	2001	quam	16	0
79051	16	2017-06-04	2018-03-20	2007	augue	65	0
21793	17	2018-01-02	2018-02-23	1993	nisl	91	0
33359	18	2017-09-04	2018-03-03	2000	ultrices	39	0
91678	19	2017-08-15	2018-01-10	2003	quam	26	0
91748	20	2017-04-30	2017-07-16	2002	ultrices	17	0
94848	21	2017-10-14	2017-05-17	2003	penatibus	36	0
21608	22	2018-03-20	2017-12-18	1996	ultrices	89	0
25797	23	2017-07-09	2017-12-06	2007	aenean	84	0
53893	24	2017-11-28	2017-05-11	1999	ante	88	0
59564	25	2018-01-21	2017-09-29	1999	nunc	65	0
90191	26	2017-08-28	2017-07-30	1994	at	78	0
23763	27	2017-11-22	2017-10-10	2012	luctus	51	0
80238	28	2017-05-06	2017-12-24	2010	pede	48	0
38326	29	2017-07-13	2018-04-10	1995	suspendisse	31	0
42535	30	2018-01-13	2017-12-09	1999	justo	30	0
4591	31	2017-12-04	2018-01-07	1990	et	24	0
48686	32	2017-11-12	2017-08-22	2006	quam	84	0
65276	33	2018-02-12	2017-09-21	1995	non	80	0
9291	34	2018-01-05	2017-11-20	1989	vulputate	70	0
578	35	2017-11-05	2017-07-24	1992	in	94	0
10009	36	2017-12-18	2018-01-02	2003	odio	65	0
53938	37	2017-08-05	2017-12-02	2012	interdum	74	0
47339	38	2018-03-08	2017-11-02	1988	aliquet	64	0
27651	39	2017-11-07	2017-05-27	2001	nibh	32	0
48445	40	2017-10-05	2017-11-15	1995	semper	18	0
79085	41	2017-05-04	2017-10-20	2011	sit	95	0
19546	42	2018-03-21	2017-11-08	1985	rhoncus	69	0
37058	43	2018-02-18	2018-01-14	1996	et	41	0
29760	44	2018-02-14	2017-06-14	1989	eleifend	46	0
20890	45	2017-11-11	2017-11-20	2001	turpis	55	0
43942	46	2018-03-22	2017-10-16	1978	ultrices	16	0
92838	47	2017-06-04	2017-07-12	2012	donec	84	0
8041	48	2018-04-23	2017-10-15	2009	nec	79	0
61320	49	2018-01-07	2017-08-22	1994	ut	47	0
33199	50	2017-09-23	2018-01-26	2004	nascetur	19	0
99091	51	2017-06-20	2018-01-12	1993	ridiculus	76	0
84840	52	2017-09-07	2017-11-22	2007	amet	71	0
16235	53	2017-09-10	2018-03-05	1997	commodo	39	0
37889	54	2018-03-20	2017-10-17	2001	a	49	0
49570	55	2017-08-15	2017-06-27	1999	pede	75	0
72067	56	2017-06-09	2017-05-06	2007	phasellus	52	0
56652	57	2017-11-17	2018-04-10	2001	parturient	5	0
26459	58	2017-11-08	2018-01-12	2010	lacus	36	0
81397	59	2017-07-04	2017-09-08	2001	integer	8	0
35819	60	2017-05-28	2017-11-17	2004	ante	28	0
62235	61	2018-03-07	2017-06-25	1990	odio	32	0
10089	62	2017-08-30	2017-09-28	2010	in	91	0
39750	63	2018-03-31	2017-09-27	2008	justo	68	0
15842	64	2017-05-26	2017-05-21	2004	turpis	100	0
4942	65	2017-05-28	2017-08-03	2008	suspendisse	56	0
79051	66	2017-09-05	2017-07-22	2011	amet	28	0
21793	67	2017-08-05	2017-08-05	2000	posuere	73	0
33359	68	2017-06-13	2017-08-25	1996	odio	56	0
91678	69	2017-07-17	2017-12-05	1972	pretium	45	0
91748	70	2018-03-12	2017-05-22	1969	et	22	0
94848	71	2018-04-19	2018-01-01	1997	diam	13	0
21608	72	2017-09-27	2018-03-10	1991	at	58	0
25797	73	2017-09-17	2017-08-30	2008	dignissim	4	0
53893	74	2017-11-24	2017-06-02	2007	erat	50	0
59564	75	2017-10-09	2017-09-24	1994	leo	59	0
90191	76	2018-01-22	2018-01-06	2008	fringilla	50	0
23763	77	2017-11-12	2018-01-27	2007	nullam	86	0
80238	78	2017-08-21	2018-02-10	1997	curae	59	0
38326	79	2017-05-04	2017-07-12	2001	dolor	22	0
42535	80	2017-06-27	2017-08-30	1994	egestas	25	0
4591	81	2017-05-28	2018-02-02	2006	eros	19	0
48686	82	2017-05-08	2018-04-28	1998	risus	19	0
65276	83	2018-03-01	2017-07-06	1974	diam	79	0
9291	84	2017-08-06	2017-12-17	2006	nulla	23	0
578	85	2017-06-04	2017-11-03	1998	orci	78	0
10009	86	2017-12-26	2017-05-24	2003	phasellus	83	0
53938	87	2017-10-07	2017-07-31	1992	augue	96	0
47339	88	2017-08-12	2017-10-31	2006	amet	88	0
27651	89	2017-11-19	2018-01-08	2009	ac	69	0
48445	90	2017-12-05	2017-09-25	2007	consequat	67	0
79085	91	2017-10-21	2017-09-11	1985	semper	72	0
19546	92	2017-08-09	2017-09-05	2004	aliquam	59	0
37058	93	2018-04-13	2017-10-31	2011	aliquet	33	0
29760	94	2017-11-06	2017-11-24	1998	mauris	3	0
20890	95	2017-11-16	2018-01-08	1993	nibh	4	0
43942	96	2017-06-10	2017-11-28	2002	blandit	71	0
92838	97	2017-08-23	2017-05-17	2000	imperdiet	28	0
8041	98	2017-11-06	2017-10-30	1988	justo	41	0
61320	99	2018-01-06	2018-04-27	2003	faucibus	54	0
\.


--
-- Data for Name: syarat_beasiswa; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY syarat_beasiswa (kode_beasiswa, syarat) FROM stdin;
99091	Nullam varius.
84840	Aliquam erat volutpat. In congue. Etiam justo.
16235	Integer non velit. Donec diam neque, vestibulum eg
37889	In est risus, auctor sed, tristique in, tempus sit
49570	Duis bibendum. Morbi non quam nec dui luctus rutru
72067	Cras non velit nec nisi vulputate nonummy. Maecena
56652	Donec semper sapien a libero. Nam dui. Proin leo o
26459	Curabitur gravida nisi at nibh. In hac habitasse p
81397	Morbi non lectus.
35819	Nulla facilisi. Cras non velit nec nisi vulputate 
62235	In hac habitasse platea dictumst.
10089	Morbi quis tortor id nulla ultrices aliquet.
39750	Etiam justo.
15842	Cras pellentesque volutpat dui. Maecenas tristique
4942	In est risus, auctor sed, tristique in, tempus sit
79051	Nunc rhoncus dui vel sem. Sed sagittis.
21793	Maecenas tincidunt lacus at velit. Vivamus vel nul
33359	Maecenas tristique, est et tempus semper, est quam
91678	Vivamus vestibulum sagittis sapien.
91748	Pellentesque viverra pede ac diam. Cras pellentesq
94848	Mauris ullamcorper purus sit amet nulla.
21608	Praesent id massa id nisl venenatis lacinia. Aenea
25797	Mauris sit amet eros. Suspendisse accumsan tortor 
53893	Nullam orci pede, venenatis non, sodales sed, tinc
59564	Nulla neque libero, convallis eget, eleifend luctu
90191	Nulla facilisi. Cras non velit nec nisi vulputate 
23763	Pellentesque at nulla.
80238	Morbi non lectus.
38326	Integer aliquet, massa id lobortis convallis, tort
42535	Integer ac leo. Pellentesque ultrices mattis odio.
4591	Morbi a ipsum. Integer a nibh.
48686	Suspendisse accumsan tortor quis turpis. Sed ante.
65276	Nunc nisl.
9291	Integer ac leo. Pellentesque ultrices mattis odio.
578	Nullam orci pede, venenatis non, sodales sed, tinc
10009	Nulla justo. Aliquam quis turpis eget elit sodales
53938	Proin at turpis a pede posuere nonummy. Integer no
47339	Nam dui.
27651	In hac habitasse platea dictumst. Aliquam augue qu
48445	Sed sagittis.
79085	In blandit ultrices enim.
19546	Morbi ut odio.
37058	Sed accumsan felis. Ut at dolor quis odio consequa
29760	Vivamus metus arcu, adipiscing molestie, hendrerit
20890	Duis consequat dui nec nisi volutpat eleifend. Don
43942	Cras non velit nec nisi vulputate nonummy.
92838	Morbi vel lectus in quam fringilla rhoncus. Mauris
8041	Sed ante. Vivamus tortor. Duis mattis egestas metu
61320	In congue.
33199	Pellentesque eget nunc. Donec quis orci eget orci 
99091	Praesent blandit lacinia erat. Vestibulum sed magn
84840	Etiam justo. Etiam pretium iaculis justo.
16235	Nulla ac enim. In tempor, turpis nec euismod scele
37889	Aliquam erat volutpat. In congue. Etiam justo.
49570	Aliquam quis turpis eget elit sodales scelerisque.
72067	Vestibulum ante ipsum primis in faucibus orci luct
56652	Cras non velit nec nisi vulputate nonummy.
26459	Praesent blandit lacinia erat. Vestibulum sed magn
81397	Fusce consequat. Nulla nisl. Nunc nisl.
35819	Suspendisse potenti.
62235	Lorem ipsum dolor sit amet, consectetuer adipiscin
10089	Nullam varius. Nulla facilisi. Cras non velit nec 
39750	Nullam orci pede, venenatis non, sodales sed, tinc
15842	Proin interdum mauris non ligula pellentesque ultr
4942	Maecenas leo odio, condimentum id, luctus nec, mol
79051	Proin leo odio, porttitor id, consequat in, conseq
21793	Donec quis orci eget orci vehicula condimentum.
33359	Maecenas rhoncus aliquam lacus. Morbi quis tortor 
91678	Phasellus id sapien in sapien iaculis congue. Viva
91748	Vestibulum ante ipsum primis in faucibus orci luct
94848	In congue. Etiam justo. Etiam pretium iaculis just
21608	Aliquam non mauris.
25797	Morbi non lectus.
53893	Pellentesque ultrices mattis odio. Donec vitae nis
59564	Donec ut mauris eget massa tempor convallis. Nulla
90191	Proin leo odio, porttitor id, consequat in, conseq
23763	Nullam molestie nibh in lectus. Pellentesque at nu
80238	Morbi sem mauris, laoreet ut, rhoncus aliquet, pul
38326	Praesent blandit. Nam nulla.
42535	Cum sociis natoque penatibus et magnis dis parturi
4591	Duis ac nibh.
48686	Nam ultrices, libero non mattis pulvinar, nulla pe
65276	Praesent blandit. Nam nulla. Integer pede justo, l
9291	Vestibulum ante ipsum primis in faucibus orci luct
578	Integer aliquet, massa id lobortis convallis, tort
10009	Morbi ut odio. Cras mi pede, malesuada in, imperdi
53938	Quisque ut erat. Curabitur gravida nisi at nibh.
47339	Ut tellus.
27651	In hac habitasse platea dictumst.
48445	Aenean sit amet justo. Morbi ut odio.
79085	Mauris lacinia sapien quis libero. Nullam sit amet
19546	Aenean fermentum. Donec ut mauris eget massa tempo
37058	Nam nulla. Integer pede justo, lacinia eget, tinci
29760	Suspendisse potenti. Cras in purus eu magna vulput
20890	Etiam pretium iaculis justo. In hac habitasse plat
43942	Curabitur in libero ut massa volutpat convallis. M
92838	In tempor, turpis nec euismod scelerisque, quam tu
8041	Nam ultrices, libero non mattis pulvinar, nulla pe
61320	Phasellus id sapien in sapien iaculis congue.
33199	Ut at dolor quis odio consequat varius. Integer ac
99091	Vivamus vestibulum sagittis sapien.
84840	Vestibulum ante ipsum primis in faucibus orci luct
16235	Maecenas pulvinar lobortis est.
37889	Ut tellus. Nulla ut erat id mauris vulputate eleme
49570	In eleifend quam a odio. In hac habitasse platea d
72067	Curabitur convallis.
56652	Pellentesque viverra pede ac diam. Cras pellentesq
26459	Pellentesque ultrices mattis odio.
81397	Nullam varius. Nulla facilisi. Cras non velit nec 
35819	Suspendisse accumsan tortor quis turpis. Sed ante.
62235	Donec semper sapien a libero. Nam dui.
10089	Lorem ipsum dolor sit amet, consectetuer adipiscin
39750	Vestibulum ante ipsum primis in faucibus orci luct
15842	Vestibulum ac est lacinia nisi venenatis tristique
4942	Nulla facilisi. Cras non velit nec nisi vulputate 
79051	Morbi vestibulum, velit id pretium iaculis, diam e
21793	Nulla tempus.
33359	Quisque porta volutpat erat.
91678	Aliquam non mauris. Morbi non lectus. Aliquam sit 
91748	Nulla mollis molestie lorem. Quisque ut erat. Cura
94848	Donec diam neque, vestibulum eget, vulputate ut, u
21608	Aenean auctor gravida sem.
25797	Vivamus tortor.
53893	Duis at velit eu est congue elementum. In hac habi
59564	Nulla nisl.
90191	Nunc purus. Phasellus in felis.
23763	Morbi sem mauris, laoreet ut, rhoncus aliquet, pul
80238	Duis ac nibh.
38326	In hac habitasse platea dictumst.
42535	Praesent blandit.
4591	Donec vitae nisi. Nam ultrices, libero non mattis 
48686	Nullam molestie nibh in lectus. Pellentesque at nu
65276	Nam ultrices, libero non mattis pulvinar, nulla pe
9291	Vivamus tortor. Duis mattis egestas metus.
578	Praesent blandit lacinia erat.
10009	Vivamus metus arcu, adipiscing molestie, hendrerit
53938	Lorem ipsum dolor sit amet, consectetuer adipiscin
47339	Phasellus id sapien in sapien iaculis congue.
27651	Quisque porta volutpat erat. Quisque erat eros, vi
48445	Morbi ut odio.
79085	Fusce posuere felis sed lacus. Morbi sem mauris, l
19546	Etiam pretium iaculis justo.
37058	Etiam vel augue.
29760	Donec diam neque, vestibulum eget, vulputate ut, u
20890	Proin at turpis a pede posuere nonummy.
43942	Cras mi pede, malesuada in, imperdiet et, commodo 
92838	Vestibulum quam sapien, varius ut, blandit non, in
8041	Integer a nibh. In quis justo. Maecenas rhoncus al
61320	Morbi ut odio.
33199	Mauris sit amet eros. Suspendisse accumsan tortor 
\.


--
-- Data for Name: tempat_wawancara; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY tempat_wawancara (kode, nama, lokasi) FROM stdin;
569	rhoncus mauris enim leo rhoncus sed vestibulum	Fakultas Matematika dan Ilmu Pengetahuan Alam
3049	lobortis sapien sapien non mi integer	Vokasi
3824	nam ultrices libero non mattis pulvinar	Fakultas Ilmu Administrasi
91	proin eu mi nulla ac enim in	Fakultas Teknik
5790	mattis nibh ligula nec sem duis	Fakultas Farmasi
3572	lacus curabitur at ipsum ac tellus	Fakultas Kedokteran
4538	sem praesent id massa id nisl	Fakultas Teknik
397	amet nulla quisque arcu libero rutrum ac	Fakultas Kedokteran Gigi
8441	in leo maecenas pulvinar lobortis est phasellus	Fakultas Matematika dan Ilmu Pengetahuan Alam
259	vitae ipsum aliquam non mauris morbi	Fakultas Teknik
2449	fringilla rhoncus mauris enim leo rhoncus	Fakultas Psikologi
7475	vel enim sit amet nunc viverra	Fakultas Teknik
6771	bibendum morbi non quam nec dui	Fakultas Ilmu Komputer
5832	suscipit a feugiat et eros vestibulum ac	Fakultas Ekonomi
4875	nulla ut erat id mauris vulputate	Fakultas Farmasi
8242	etiam pretium iaculis justo in hac habitasse	Fakultas Ilmu Sosial dan Ilmu Politik
4205	nulla quisque arcu libero rutrum ac lobortis	Fakultas Ilmu Administrasi
371	ut erat id mauris vulputate elementum	Fakultas Kesehatan Masyarakat
6864	id nisl venenatis lacinia aenean sit amet	Fakultas Kedokteran
7203	quam pharetra magna ac consequat metus	Fakultas Ilmu Keperawatan
7641	bibendum felis sed interdum venenatis turpis enim	Fakultas Matematika dan Ilmu Pengetahuan Alam
5881	velit id pretium iaculis diam erat	Fakultas Ilmu Sosial dan Ilmu Politik
8208	pharetra magna ac consequat metus sapien ut	Fakultas Ilmu Komputer
512	duis aliquam convallis nunc proin at turpis	Fakultas Kedokteran
4830	velit nec nisi vulputate nonummy tincidunt	Fakultas Ekonomi
\.


--
-- Data for Name: wawancara; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY wawancara (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal, kode_tempat_wawancara) FROM stdin;
1	99091	2018-03-31 00:00:00	569
2	84840	2018-02-03 00:00:00	3049
3	16235	2017-11-01 00:00:00	3824
4	37889	2017-08-21 00:00:00	91
5	49570	2018-02-01 00:00:00	5790
6	72067	2017-08-29 00:00:00	3572
7	56652	2017-07-02 00:00:00	4538
8	26459	2018-04-24 00:00:00	397
9	81397	2017-12-30 00:00:00	8441
10	35819	2017-07-03 00:00:00	259
11	62235	2018-01-23 00:00:00	2449
12	10089	2017-10-27 00:00:00	7475
13	39750	2017-09-15 00:00:00	6771
14	15842	2018-04-27 00:00:00	5832
15	4942	2018-01-24 00:00:00	4875
16	79051	2018-03-16 00:00:00	8242
17	21793	2017-09-19 00:00:00	4205
18	33359	2017-07-27 00:00:00	371
19	91678	2017-09-02 00:00:00	6864
20	91748	2018-02-22 00:00:00	7203
21	94848	2017-12-22 00:00:00	7641
22	21608	2018-03-27 00:00:00	5881
23	25797	2018-03-09 00:00:00	8208
24	53893	2017-05-21 00:00:00	512
25	59564	2017-08-31 00:00:00	4830
26	90191	2018-03-17 00:00:00	569
27	23763	2018-04-11 00:00:00	3049
28	80238	2017-06-29 00:00:00	3824
29	38326	2017-06-09 00:00:00	91
30	42535	2018-01-12 00:00:00	5790
31	4591	2017-10-27 00:00:00	3572
32	48686	2017-11-29 00:00:00	4538
33	65276	2017-12-04 00:00:00	397
34	9291	2017-11-25 00:00:00	8441
35	578	2017-11-05 00:00:00	259
36	10009	2018-01-01 00:00:00	2449
37	53938	2018-01-24 00:00:00	7475
38	47339	2017-07-12 00:00:00	6771
39	27651	2017-05-02 00:00:00	5832
40	48445	2017-07-26 00:00:00	4875
41	79085	2017-05-23 00:00:00	8242
42	19546	2017-07-24 00:00:00	4205
43	37058	2017-06-08 00:00:00	371
44	29760	2018-04-19 00:00:00	6864
45	20890	2017-05-23 00:00:00	7203
46	43942	2017-07-03 00:00:00	7641
47	92838	2018-01-15 00:00:00	5881
48	8041	2018-03-07 00:00:00	8208
49	61320	2017-07-10 00:00:00	512
50	33199	2017-06-07 00:00:00	4830
51	99091	2018-01-01 00:00:00	569
52	84840	2017-07-19 00:00:00	3049
53	16235	2017-07-01 00:00:00	3824
54	37889	2018-04-26 00:00:00	91
55	49570	2017-12-30 00:00:00	5790
56	72067	2017-08-29 00:00:00	3572
57	56652	2017-11-16 00:00:00	4538
58	26459	2018-03-06 00:00:00	397
59	81397	2017-08-08 00:00:00	8441
60	35819	2017-12-26 00:00:00	259
61	62235	2017-08-23 00:00:00	2449
62	10089	2017-09-15 00:00:00	7475
63	39750	2018-04-14 00:00:00	6771
64	15842	2017-12-15 00:00:00	5832
65	4942	2018-02-04 00:00:00	4875
66	79051	2017-06-21 00:00:00	8242
67	21793	2017-07-25 00:00:00	4205
68	33359	2017-12-12 00:00:00	371
69	91678	2017-07-09 00:00:00	6864
70	91748	2017-06-07 00:00:00	7203
71	94848	2017-06-15 00:00:00	7641
72	21608	2017-12-31 00:00:00	5881
73	25797	2018-03-09 00:00:00	8208
74	53893	2017-10-11 00:00:00	512
75	59564	2017-09-20 00:00:00	4830
76	90191	2017-05-01 00:00:00	569
77	23763	2018-01-08 00:00:00	3049
78	80238	2018-02-15 00:00:00	3824
79	38326	2017-06-05 00:00:00	91
80	42535	2018-04-09 00:00:00	5790
81	4591	2018-04-19 00:00:00	3572
82	48686	2018-02-09 00:00:00	4538
83	65276	2017-12-31 00:00:00	397
84	9291	2017-10-25 00:00:00	8441
85	578	2017-12-01 00:00:00	259
86	10009	2018-04-25 00:00:00	2449
87	53938	2017-10-04 00:00:00	7475
88	47339	2018-02-10 00:00:00	6771
89	27651	2017-07-25 00:00:00	5832
90	48445	2018-04-05 00:00:00	4875
91	79085	2018-03-05 00:00:00	8242
92	19546	2017-06-24 00:00:00	4205
93	37058	2017-08-05 00:00:00	371
94	29760	2017-09-27 00:00:00	6864
95	20890	2017-05-19 00:00:00	7203
96	43942	2017-10-09 00:00:00	7641
97	92838	2017-07-19 00:00:00	5881
98	8041	2017-08-28 00:00:00	8208
99	61320	2017-09-06 00:00:00	512
100	33199	2018-02-07 00:00:00	4830
\.


--
-- Data for Name: yayasan; Type: TABLE DATA; Schema: simbion; Owner: db041
--

COPY yayasan (no_sk_yayasan, email, nama, no_telp_cp, nomor_identitas_donatur) FROM stdin;
809/0V/HXOA/F9V/2010	mrenneke0@google.nl	Jatri	+142887987933	2
715/5B/LBZS/JUL/2011	theino1@gmpg.org	Digitube	+775619381591	3
611/8P/LWNX/7V8/2010	aouchterlony2@twitter.com	Leenti	+417773796308	4
411/0H/TTTN/T46/2015	vwildash3@tinyurl.com	Brainsphere	+614824788071	5
494/0F/XYOK/MLA/2010	mlevet4@usda.gov	Brightdog	+321862436935	6
048/9E/KWEZ/AAD/2017	jzarb5@amazon.de	Layo	+987327337656	7
388/3C/WNQS/SHS/2017	htudge6@lycos.com	Omba	+200335889141	8
118/9B/LVMK/VJE/2014	rgherarducci7@youtu.be	Wordware	+279206018058	10
905/60/XJQE/1AI/2013	hleighfield8@ftc.gov	Vinder	+744957422042	15
103/31/HVNE/LHD/2019	cbraime9@baidu.com	Dynabox	+151409057566	17
250/92/TDDG/9N1/2016	lcristofanoa@sitemeter.com	Jabbertype	+489119543948	18
069/2V/JIWY/C68/2017	tlardezb@wufoo.com	Yata	+518784873231	20
878/0J/VHLJ/TIR/2012	sbrandic@china.com.cn	Oba	+260641611074	22
654/2B/UHZA/LH0/2015	jvaneedend@a8.net	Jatri	+157433545538	23
434/8U/YDNK/7W1/2011	jmatisoffe@mail.ru	Skippad	+359502056658	25
904/3N/TEBV/86K/2015	sdecleenf@miitbeian.gov.cn	Eabox	+018677923166	26
406/8V/MSIM/70I/2013	hbighamg@wix.com	Voonyx	+756775405054	28
570/45/MCUL/MCR/2014	cimlinh@dailymail.co.uk	Skinix	+213227880710	29
350/0X/DPRJ/61S/2010	gyeskini@exblog.jp	Zoombox	+053670786296	32
286/8A/XJIT/CXZ/2017	hlowselyj@cocolog-nifty.com	Gabtune	+131457575257	35
643/89/UOPO/RSA/2010	aduchasteauk@yale.edu	Photobug	+771008541033	36
752/3M/THMT/B5M/2012	dburlandl@dot.gov	Photobug	+223001154975	37
384/0T/ZYGO/PUY/2019	vandreottim@desdev.cn	Meezzy	+376844580739	38
752/1W/EZWB/EIP/2010	agartann@meetup.com	Jabberstorm	+001849039393	40
784/0X/MVDB/B0M/2016	scraighallo@icq.com	Babbleopia	+441209680979	41
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: donatur donatur_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY donatur
    ADD CONSTRAINT donatur_pkey PRIMARY KEY (nomor_identitas);


--
-- Name: individual_donor individual_donor_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY individual_donor
    ADD CONSTRAINT individual_donor_pkey PRIMARY KEY (nik);


--
-- Name: mahasiswa mahasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY mahasiswa
    ADD CONSTRAINT mahasiswa_pkey PRIMARY KEY (npm);


--
-- Name: pembayaran pembayaran_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_pkey PRIMARY KEY (urutan, kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm);


--
-- Name: pendaftaran pendaftaran_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_pkey PRIMARY KEY (no_urut, kode_skema_beasiswa, npm);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (username);


--
-- Name: pengumuman pengumuman_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_pkey PRIMARY KEY (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username);


--
-- Name: riwayat_akademik riwayat_akademik_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY riwayat_akademik
    ADD CONSTRAINT riwayat_akademik_pkey PRIMARY KEY (no_urut, npm);


--
-- Name: skema_beasiswa_aktif skema_beasiswa_aktif_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY skema_beasiswa_aktif
    ADD CONSTRAINT skema_beasiswa_aktif_pkey PRIMARY KEY (kode_skema_beasiswa, no_urut);


--
-- Name: skema_beasiswa skema_beasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY skema_beasiswa
    ADD CONSTRAINT skema_beasiswa_pkey PRIMARY KEY (kode);


--
-- Name: syarat_beasiswa syarat_beasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY syarat_beasiswa
    ADD CONSTRAINT syarat_beasiswa_pkey PRIMARY KEY (kode_beasiswa, syarat);


--
-- Name: tempat_wawancara tempat_wawancara_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY tempat_wawancara
    ADD CONSTRAINT tempat_wawancara_pkey PRIMARY KEY (kode);


--
-- Name: wawancara wawancara_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_pkey PRIMARY KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal);


--
-- Name: yayasan yayasan_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY yayasan
    ADD CONSTRAINT yayasan_pkey PRIMARY KEY (no_sk_yayasan);


--
-- Name: pembayaran total_pembayaran_trigger; Type: TRIGGER; Schema: simbion; Owner: db041
--

CREATE TRIGGER total_pembayaran_trigger AFTER INSERT ON pembayaran FOR EACH ROW EXECUTE PROCEDURE total_pembayaran_ft();


--
-- Name: admin admin_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: donatur donatur_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY donatur
    ADD CONSTRAINT donatur_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username);


--
-- Name: individual_donor individual_donor_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY individual_donor
    ADD CONSTRAINT individual_donor_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mahasiswa mahasiswa_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY mahasiswa
    ADD CONSTRAINT mahasiswa_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pembayaran pembayaran_kode_skema_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_kode_skema_beasiswa_fkey FOREIGN KEY (kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES skema_beasiswa_aktif(kode_skema_beasiswa, no_urut) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pembayaran pembayaran_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pendaftaran pendaftaran_no_urut_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_no_urut_fkey FOREIGN KEY (no_urut, kode_skema_beasiswa) REFERENCES skema_beasiswa_aktif(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pendaftaran pendaftaran_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengumuman pengumuman_no_urut_skema_beasiswa_aktif_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_no_urut_skema_beasiswa_aktif_fkey FOREIGN KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa) REFERENCES skema_beasiswa_aktif(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengumuman pengumuman_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_akademik riwayat_akademik_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY riwayat_akademik
    ADD CONSTRAINT riwayat_akademik_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: skema_beasiswa_aktif skema_beasiswa_aktif_kode_skema_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY skema_beasiswa_aktif
    ADD CONSTRAINT skema_beasiswa_aktif_kode_skema_beasiswa_fkey FOREIGN KEY (kode_skema_beasiswa) REFERENCES skema_beasiswa(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: skema_beasiswa skema_beasiswa_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY skema_beasiswa
    ADD CONSTRAINT skema_beasiswa_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: syarat_beasiswa syarat_beasiswa_kode_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY syarat_beasiswa
    ADD CONSTRAINT syarat_beasiswa_kode_beasiswa_fkey FOREIGN KEY (kode_beasiswa) REFERENCES skema_beasiswa(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wawancara wawancara_kode_tempat_wawancara_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_kode_tempat_wawancara_fkey FOREIGN KEY (kode_tempat_wawancara) REFERENCES tempat_wawancara(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wawancara wawancara_no_urut_skema_beasiswa_aktif_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_no_urut_skema_beasiswa_aktif_fkey FOREIGN KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa) REFERENCES skema_beasiswa_aktif(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: yayasan yayasan_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db041
--

ALTER TABLE ONLY yayasan
    ADD CONSTRAINT yayasan_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

