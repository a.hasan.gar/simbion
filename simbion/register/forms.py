from django import forms

text_input_attrs = {
    'type': 'text',
    'class': 'form-textinput',
}

number_input_attrs = {
    'type': 'number',
    'class': 'form-numberinput',
}


class RegisterForm(forms.Form):
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    npm = forms.CharField(label='NPM', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    nama_lengkap = forms.CharField(label='Nama Lengkap', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    nomor_telepon = forms.CharField(label='Nomor Telepon', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    alamat_tempat_tinggal = forms.CharField(label='Alamat Tempat Tinggal', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    alamat_domisili = forms.CharField(label='Alamat Domisili', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    instansi_bank = forms.CharField(label='Instansi Bank', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    nomor_rekening = forms.CharField(label='Nomor Rekening', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
    nama_pemilik = forms.CharField(label='Nama Pemilik', required=True, max_length=50, widget=forms.TextInput(attrs=text_input_attrs))
