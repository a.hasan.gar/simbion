from django.shortcuts import render
from .forms import RegisterForm

response = {}


def index(request):
    response['author'] = 'Emil Farisan Singgih'
    response['form_wawancara'] = RegisterForm
    html = 'register/register.html'
    return render(request, html, response)
